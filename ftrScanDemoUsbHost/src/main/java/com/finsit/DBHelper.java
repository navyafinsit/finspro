package com.finsit;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class DBHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "crimes.db";
	public static final String CRIMES_TABLE_NAME = "crimes";
	public static final String CRIMES_COLUMN_ID = "id";
	public static final String CRIMES_YEAR = "year";
	public static final String CRIMES_MONTH = "month";
	public static final String CRIMES_DAY = "day";
	public static final String CRIMES_TIME = "time";
	public static final String CRIMES_LATITUDE = "latitude";
	public static final String CRIMES_LONGITUDE = "longitude";
	public static final String CRIMES_TITLE = "title";
	public static final String CRIMES_DETAILS = "details";
	public static final String CRIMES_USER_ID = "userid";
	public static final String CRIMES_FILE_NAME = "fileName";
	public static final byte[] image=null;
	public static final String CRIMES_STATUS = "status";
	private HashMap hp;
	
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, 1);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		String createTable = "create table crimes"
				+ "(id integer primary key,year text,month text,day text, time text,latitude text,longitude text,title text,details text,userid text,fileName text,image blob,fingerImage blob,status text)";
		System.out.println(createTable);
		db.execSQL(createTable);
		System.out.println("Database created successfully");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS crimes");
		onCreate(db);
		System.out.println("Database is dropped");
	}

	public boolean insertCrime(String year, String month, String day,String time, String latitude, String longitude, String title,String details,String fileName,byte[] img,byte[] imgFinger,String status) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("year", year);
		contentValues.put("month", month);
		contentValues.put("day", day);
		contentValues.put("time", time);
		contentValues.put("latitude", latitude);
		contentValues.put("longitude", longitude);
		contentValues.put("title", title);
		contentValues.put("details", details);
		contentValues.put("userid", "1");
		contentValues.put("fileName", fileName);
		contentValues.put("image", img);
		contentValues.put("fingerImage",imgFinger);
		contentValues.put("status",status);
		System.out.println("Crime Record is:");
		System.out.println(year+","+month+","+day+","+time+","+latitude+","+longitude+","+title+","+details+","+fileName+","+img+","+imgFinger+","+status);
		db.insert("crimes", null, contentValues);
		System.out.println("Crime inserted successfully");
		getAllCotacts();
		return true;
	}

	public Cursor getData(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor res = db.rawQuery("select * from crimes where id=" + id + "",
				null);
		return res;
	}

	public int numberOfRows() {
		SQLiteDatabase db = this.getReadableDatabase();
		int numRows = (int) DatabaseUtils.queryNumEntries(db, CRIMES_TABLE_NAME);
		return numRows;
	}
	public boolean updateCrime(Integer id,String year, String month, String day,String time, String latitude, String longitude, String title,String details,String fileName,byte[] img,String status) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("year", year);
		contentValues.put("month", month);
		contentValues.put("day", day);
		contentValues.put("time", time);
		contentValues.put("latitude", latitude);
		contentValues.put("longitude", longitude);
		contentValues.put("title", title);
		contentValues.put("details", details);
		contentValues.put("userid", "1");
		contentValues.put("fileName", fileName);
		contentValues.put("image", img);
		contentValues.put("status",status);
		System.out.println("Crime Record is:");
		System.out.println(year+","+month+","+day+","+time+","+latitude+","+longitude+","+title+","+details+","+fileName+","+img+","+status);
		db.update("crimes", contentValues, "id = ? ",new String[] { Integer.toString(id) });
		return true;
	}
	public boolean updateCrime(Integer id, String year, String month,String day, String time, String latitude, String longitude,String title, String details,String status) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues contentValues = new ContentValues();
		contentValues.put("year", year);
		contentValues.put("month", month);
		contentValues.put("day", day);
		contentValues.put("time", time);
		contentValues.put("latitude", latitude);
		contentValues.put("longitude", longitude);
		contentValues.put("title", title);
		contentValues.put("details", details);
		contentValues.put("status",status);		
		db.update("crimes", contentValues, "id = ? ",new String[] { Integer.toString(id) });
		return true;
	}

	public Integer deleteCrime(Integer id) {
		SQLiteDatabase db = this.getWritableDatabase();
		return db.delete("crimes", "id = ? ",
				new String[] { Integer.toString(id) });
	}

	public ArrayList<CrimeDetailsBean> getAllCotacts() {
		ArrayList<CrimeDetailsBean> array_list = new ArrayList<CrimeDetailsBean>();
		// hp = new HashMap();
		CrimeDetailsBean cdb;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor res = db.rawQuery("select * from crimes", null);
		res.moveToFirst();
		System.out.println("**********LIST OF RECORD IN THE DATABASE**********");
		while (res.isAfterLast() == false) {
			cdb=new CrimeDetailsBean();
			cdb.setId(res.getInt(0));
			cdb.setYear(res.getString(1));
			cdb.setMonth(res.getString(2));
			cdb.setDay(res.getString(3));
			cdb.setTime(res.getString(4));
			cdb.setLatitude(res.getString(5));
			cdb.setLongitude(res.getString(6));
			cdb.setTitle(res.getString(7));
			cdb.setDetails(res.getString(8));
			cdb.setUserid(res.getInt(9));
			cdb.setFileName(res.getString(10));
			cdb.setImg(res.getBlob(11));
			cdb.setImg(res.getBlob(12));
			cdb.setStatus(res.getString(13));
			array_list.add(cdb);
			System.out.println(res.getString(0)+","+res.getString(1)+","+res.getString(2)+","+res.getString(3)+","+res.getString(4)+","+res.getString(5)+","+res.getString(6)+","+res.getString(7)+","+res.getString(8)+","+res.getString(9)+","+res.getString(10)+","+res.getBlob(11)+","+res.getBlob(12)+","+res.getString(13));
			res.moveToNext();
		}
		return array_list;
	}
}