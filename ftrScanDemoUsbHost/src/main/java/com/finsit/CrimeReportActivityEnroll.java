package com.finsit;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.finsit.R;


@SuppressWarnings("unused")
public class CrimeReportActivityEnroll extends Activity implements SampleDialogFragment.SampleDialogListener, UserDialog.UserDialogListener {
	private static final String TAG = CrimeReportActivityEnroll.class.getSimpleName();
	
	ImageView img_fp_src; 
	TextView tvInfo, tvVer, tvDevice,deviceIdFromPCATS;	
	Button btnCapture1,btnVerifyTemplate,btnOpenDevice;
	EditText inputDescription;
	//Button moreButton;
	DialogFragment sampleDialogFragment;
	DialogFragment serverProcessDialogFragment;
	UserDialog userDialog;
	String imageUrl;
	String description;
	String address;
	String phoneNo;
	String criminalDescription;
	String deviceId;
	private boolean					bCapturedFirst, bAutoOn = false;
	public static final int QUALITY_LIMIT = 80;
	Context myContext;
	SharedPreferences data;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
			myContext = createPackageContext("com.quad.track",Context.CONTEXT_IGNORE_SECURITY);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			Toast.makeText(getApplicationContext(), "Please Install PCATS Application",Toast.LENGTH_LONG).show();
			finish();
			return;
		
		}
        
        
        /*Intent enrollIntent=getIntent();
        Bitmap bitmap=(Bitmap)enrollIntent.getParcelableExtra("BitmapImage");
        //img_fp_src.setImageBitmap(bitmap);
        if(bitmap!=null)
        	{
        		Toast.makeText(getApplicationContext(), bitmap.getByteCount(), 10).show();
        	}*/
        
        
      
        
        
        initView();
        initData();
        
    }
    
    /**
     * void
     */
    public void initView(){
    
    	setContentView(R.layout.fingerprint_upload_enroll);
    	
    	 /* if(getIntent().hasExtra("byteArray")) {
          	
          	Bitmap b = BitmapFactory.decodeByteArray(
          	    getIntent().getByteArrayExtra("byteArray"),0,getIntent().getByteArrayExtra("byteArray").length);        
          		img_fp_src.setImageBitmap(b);
          	}*/
    	
    	Bitmap bmap=(Bitmap)getIntent().getParcelableExtra("BitmapImage");
    	img_fp_src = (ImageView)findViewById(R.id.img_fp_src);
    	img_fp_src.setBackgroundColor(Color.argb(255, 255, 255, 255));
    	img_fp_src.setImageBitmap(bmap);
    	tvInfo = (TextView) findViewById(R.id.textInfo);
    	//tvVer = (TextView) findViewById(R.id.textVer);
    	tvDevice = (TextView) findViewById(R.id.textDevice);
    	
/*        btnCapture1 = (Button) findViewById(R.id.btnCapture1);
    	btnCapture1.setEnabled(false);*/
    	
    	//btnOpenDevice=(Button) findViewById(R.id.btnOpenDevice);
    	btnVerifyTemplate = (Button) findViewById(R.id.btnVerifyIso);
    	inputDescription=(EditText) findViewById(R.id.description);
		data = myContext.getSharedPreferences("datastore", Context.MODE_PRIVATE);
		deviceId=data.getString("deviceid",null);
		if(deviceId!=null){
			System.out.println("DEVICE ID IS:"+deviceId);
			deviceIdFromPCATS=(TextView) findViewById(R.id.deviceIdFromPCATS);
			deviceIdFromPCATS.setText("Device:"+deviceId.toString());
			tvInfo.setText("Enroll Fingerprint");
		}
		else{
			Toast.makeText(getApplicationContext(), "Please Install PCATS Application",Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		
    	//btnVerifyTemplate.setEnabled(false);
    	//moreButton=(Button)findViewById(R.id.moreButton);
    	//moreButton.setEnabled(false);
/*    	moreButton.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				System.out.println("Entered into onclick method");
				if(criminalName!=null){
					Intent intent = new Intent(getApplicationContext(),ShowCirminalInfo.class);
				    intent.putExtra("criminalName",criminalName);
				    intent.putExtra("templateURL", templateURL);
				    intent.putExtra("address", address);
				    intent.putExtra("phoneNo", phoneNo);
				    startActivity(intent);
				    finish();
				}
				else{
					Toast.makeText(getApplicationContext(), "No Matches Found For This Finger Print", Toast.LENGTH_LONG).show();
				}
				
			}
		});*/
    	

    	
    
    }
    public void initData(){

    	    	
    	sampleDialogFragment = new SampleDialogFragment();
    	userDialog = new UserDialog();
    	serverProcessDialogFragment=new SampleDialogFragment();
    	
    }

    
   
    
    
    
	/* (non-Javadoc)
	 * @see com.nitgen.SDK.AndroidBSP.SampleDialogFragment.SampleDialogListener#onClickStopBtn(android.app.DialogFragment)
	 */
	public void onClickStopBtn(DialogFragment dialogFragment) {

		bAutoOn = false;
		sampleDialogFragment.dismiss();

	}

	
	
	

    
    
	
    /**
     * void
     */
    public void OnBtnVerifyIso(View target){
    	
    	//Toast.makeText(getApplicationContext(), img_fp_src.getDrawable(), 5).show();
    	try{
    	BitmapDrawable bitmapDrawable = ((BitmapDrawable) img_fp_src.getDrawable());
    	Bitmap bitmap = bitmapDrawable.getBitmap();
    	ByteArrayOutputStream stream = new ByteArrayOutputStream();
    	bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
    	bitmap.setDensity(500);
    	byte[] imageInByte = stream.toByteArray();
    	//ByteArrayInputStream bis = new ByteArrayInputStream(imageInByte);
    	FileOutputStream fos = new FileOutputStream("/storage/sdcard0/template.dat");
    	fos.write(imageInByte);
    	fos.close();
    	criminalDescription=inputDescription.getText().toString().replace(" ","%20");
    	serverProcessDialogFragment.show(getFragmentManager(), "DIALOG_TYPE_PROGRESS");		
    	UploadTask task=new UploadTask();
    	task.execute("/storage/sdcard0/template.dat");
    	
    	
    	//tvInfo.setText(output);
    	//Toast.makeText(getApplicationContext(), output, Toast.LENGTH_LONG).show();
    	}catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
    }
    
    /**
     * void
     */
  
    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

    	/*MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_android__demo, menu);
    	*/
    	return true;
    }
    

		
	
	private class  UploadTask extends AsyncTask<String, Void, String> {
		String output=null;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}
		
	    @Override
	    protected String doInBackground(String... urls) {
	        for (String url : urls) {
	            output = upload(url);
	           
	        }
	       /* if(output!=null){
	        	onPostExecute(output);
	        }*/
	        return output;
	    }
	    
	   
	    
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//String criminalName = null;
			//final ImageView imageView = (ImageView) findViewById(R.id.criminalImage);
			//final TextView nameOfTheCriminal=(TextView) findViewById(R.id.criminalName);
			System.out.println("Entered into on post executes"+result);
			
		    if(result!=null && "".equals(result.trim()))
		    {
		    	tvInfo.setText("Enroll Failed");
		    	tvInfo.setBackgroundColor(Color.RED);
		    	inputDescription.setText("");
				//moreButton.setEnabled(true);
				//moreButton.setBackgroundColor(Color.RED);
				serverProcessDialogFragment.dismiss();
				return;
		    }
			//tvInfo.setText(output);
			try {
				if(result!=null)
				{
					JSONArray jArray=new JSONArray(result);
					JSONObject obj = null;
					if(jArray.length()>0){
						obj=jArray.getJSONObject(0);
						System.out.println("JSON OBJECT IS"+obj);
						imageUrl=obj.getString("imageUrl");
						description=obj.getString("description");
					}
					//System.out.println("json recieved string:"+jsonString);
					//Blob blob = (Blob) obj.get("image");
					//int blobLength = (int) blob.length(); 
					//final byte[] imageBytes = blob.getBytes(1, blobLength);
					//final byte[] imageBytes =(byte[]) obj.get("image");
					//criminalName=obj.getString("CriminalName");
					if(imageUrl!=null){
						tvInfo.setText("Enrolled with image:"+imageUrl+" and Description:"+description);
						tvInfo.setBackgroundColor(Color.GREEN);
						inputDescription.setText("");
						btnVerifyTemplate.setEnabled(false);
						//img_fp_src.setVisibility(0);
						//moreButton.setEnabled(true);
						//moreButton.setBackgroundColor(Color.GREEN);
					}
				}		
				
			
				/*tvInfo.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						try {
						try {// TODO Auto-generated method stub
						setContentView(R.layout.more_information);
						ByteArrayInputStream imageStream = new ByteArrayInputStream(imageBytes);
						Bitmap theImage= BitmapFactory.decodeStream(imageStream);
						URL url = new URL(address);
						InputStream content = (InputStream)url.getContent();
						Drawable d = Drawable.createFromStream(content , "src"); 
						iv.setImageDrawable(d)
						imageView.setImageBitmap(theImage);

						//nameOfTheCriminal.setText(obj.getString("CriminalName"));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});*/
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e2) {
				System.out.println("exception occured");
				e2.printStackTrace();
			}
			serverProcessDialogFragment.dismiss();
		
		}
		public String convertStreamToString(InputStream is) throws Exception {

		    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		    StringBuilder sb = new StringBuilder();
		    String line = null;
		    while ((line = reader.readLine()) != null) {
		        sb.append(line + "\n");
		    }
		    is.close();
		    return sb.toString();
		}
		public String upload(String i_file) 
		{
			String responseString = null;
			
			try
			{
				System.out.println("ENTERED INTO UPLOAD PICTURE TO SERVER");
				if(criminalDescription!=null && deviceId!=null){
			    HttpClient httpclient = new DefaultHttpClient();
			    httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			    String imagePath="Template"+System.currentTimeMillis();
/*
			    HttpPost httppost = new HttpPost("http://134.119.179.70:9091/finsprows/EnrollPictureFromClient?imageUrl="+imagePath+"&criminalDescription="+criminalDescription+"&deviceId="+deviceId);
*/
/*
				HttpPost httppost = new HttpPost("http://198.27.68.95:9091/finsprows/EnrollPictureFromClient?imageUrl="+imagePath+"&criminalDescription="+criminalDescription+"&deviceId="+deviceId);
*/

				HttpPost httppost = new HttpPost(IPInfo.HTTP_IPADDRESS+"/finsprows/EnrollPictureFromClient?imageUrl="+imagePath+"&criminalDescription="+criminalDescription+"&deviceId="+deviceId);

				File file = new File(i_file);
			    MultipartEntity mpEntity = new MultipartEntity();
			    ContentBody cbFile = new FileBody(file, "image/jpeg");
			    mpEntity.addPart("userfile", cbFile);
			    httppost.setEntity(mpEntity);
			    System.out.println("executing request " + httppost.getRequestLine());
			    HttpResponse response = httpclient.execute(httppost);
			    HttpEntity resEntity = response.getEntity();
			    InputStream stream=resEntity.getContent();
			    responseString = convertStreamToString(stream);
			    System.out.println("RESPONSE STRING IS:"+responseString);
			    System.out.println(response.getStatusLine());
			    if (resEntity != null) {
			      resEntity.consumeContent();
			    }
			    httpclient.getConnectionManager().shutdown();
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			return responseString;
		    
		}
		
		
	}




	public void onClickPositiveBtn(DialogFragment dialogFragment, String id) {
		// TODO Auto-generated method stub
		
	}

}

   