package com.finsit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public  class UsersListViewActivity extends Activity {
    ListView displayList;

//List<UserModel> userModel;
 //   private UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.offmode_main);

        SqliteHelper sqliteHelper=new SqliteHelper(this);

        final List<UserModel> userList=sqliteHelper.getAllUsers();

        CustomUserAdapter adapter = new
                CustomUserAdapter(UsersListViewActivity.this, userList);
        displayList =(ListView)findViewById(R.id.list);
        displayList.setAdapter(adapter);
        displayList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                SharedPreferences data = getSharedPreferences("datastore", Context.MODE_PRIVATE);
                String deviceId=data.getString("deviceid",null);
                Intent intent = new Intent(UsersListViewActivity.this, UserDetailViewActivity.class);
                intent.putExtra("userObject", (Parcelable) userList.get(position));
                intent.putExtra("deviceId",deviceId);
                startActivity(intent);

                //Toast.makeText(UsersListViewActivity.this, "You Clicked at " +userList.get(position).getName(), Toast.LENGTH_SHORT).show();

            }
        });

    }

}