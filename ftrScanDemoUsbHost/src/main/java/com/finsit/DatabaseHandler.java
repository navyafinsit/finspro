package com.finsit;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {

	
	private static final int DATABASE_VERSION = 1;

	
	private static final String DATABASE_NAME = "backupdb";  

	
	private static final String TABLE_BACKUPDATA = "backupdata";

	
	private static final String KEY_ID = "id";
	private static final String LATITUDE= "latitude";
	private static final String LONGITUDE="longitude";
	private static final String DEVICEID="deviceid";
	private static final String CURRENTDATETIMESTRING="currentdatetimestring";
	private static final String DISTANCE="distance";
	private static final String BATTERYLEVEL="battery";
	
	
	
	
	private static final String TABLE_DATA = "alerts";
	

	
	private static final String KEY_ALERT_ID = "id";
	private static final String KEY_STATUS = "status";
	private static final String KEY_TIME = "time";
	private static final String KEY_ALERTTYPE = "alerttype";

	

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_BACKUPDATA_TABLE = "CREATE TABLE " + TABLE_BACKUPDATA + "("
				+ KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + LATITUDE + " DOUBLE,"+ LONGITUDE + " DOUBLE,"+ DEVICEID + " TEXT,"+ CURRENTDATETIMESTRING + " TEXT,"+ DISTANCE + " DOUBLE,"+ BATTERYLEVEL + " DOUBLE)";
		
		String CREATE_DATA_TABLE = "CREATE TABLE " + TABLE_DATA + "("
				+ KEY_ALERT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_STATUS + " text,"+ KEY_TIME + " text,"+ KEY_ALERTTYPE +" text)";
		
		db.execSQL(CREATE_BACKUPDATA_TABLE);
		db.execSQL(CREATE_DATA_TABLE);
		
	}

	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_BACKUPDATA);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_DATA);
		onCreate(db);
	}

	
	public void addBackupData(double latitude, double longitude, String deviceId, String datetime, double distance, double batteryLevel) {
			SQLiteDatabase db = null;
			try {
				db = this.getWritableDatabase();

				ContentValues values = new ContentValues();
				values.put(LATITUDE, latitude);
				values.put(LONGITUDE, longitude);
				values.put(DEVICEID, deviceId);
				values.put(CURRENTDATETIMESTRING, datetime);
				values.put(DISTANCE, distance);
				values.put(BATTERYLEVEL, batteryLevel);



				db.insert(TABLE_BACKUPDATA, null, values);

			} catch (Exception e) {

			} finally {
				if(db != null) {
					db.close();
				}
			}
	}


	
	public int getCountData() {
		
		SQLiteDatabase db = null;
				
		try {
			String selectQueryCount = "SELECT  id FROM " + TABLE_BACKUPDATA;
			db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQueryCount, null);
			int countValue = 0;
			countValue = cursor.getCount();
						
			double a = (double)countValue/ 10;
			int finalCount = 0;
		
			if(a%1 != 0) {
				int c = (int)a;
				finalCount = c + 1;
			} else {
				finalCount = (int)a;
			}
			
			return finalCount;
			
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally {
			if(db != null) {
				db.close();
			}
		}
		
		
		
	}
	
	
	
	public String getChunkData() {
		SQLiteDatabase db = null;
		
		try {
			String selectQuery = "SELECT  * FROM " + TABLE_BACKUPDATA +" ORDER BY "+KEY_ID+" LIMIT 100";
			
			db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			
			String text = "";
			
			
			
			if (cursor.moveToFirst()) {
				do {
					
					double latitude = cursor.getDouble(1);
					double longitude = cursor.getDouble(2);
					String deviceId = cursor.getString(3);
					String dateTime = cursor.getString(4);
					double distance = cursor.getDouble(5);
					double batteryLevel = cursor.getDouble(6);
					
					
					text = text+ latitude+"@"+longitude+"@"+deviceId+"@"+dateTime+"@"+distance+"@"+batteryLevel+"!"; 
					
					
				} while (cursor.moveToNext());
			}
					
			
			return text;
			
		} catch (Exception e) {
			return "";
		} finally {
			if(db != null) {
				db.close();
			}
		}
	}
	
	
	public void deleteChunkData() {
		
		SQLiteDatabase db = null;
		try {
			db = this.getWritableDatabase();
			
			db.execSQL("DELETE FROM " + TABLE_BACKUPDATA+" where id in (SELECT "+KEY_ID+" FROM " + TABLE_BACKUPDATA+" ORDER BY "+KEY_ID+" LIMIT 100)");
		} catch (SQLException e) {
			
		}  finally {
			if(db != null) {
				db.close();
			}
		}
		
		
	}
	
	
	public void delete() {
		SQLiteDatabase db = null;
		try {
			db = this.getWritableDatabase();
			db.execSQL("DELETE FROM "+TABLE_BACKUPDATA);
		} catch (SQLException e) {
			
		}  finally {
			if(db != null) {
				db.close();
			}
		}
	}
	
	
	
	
		public void addData(String status, String time, String AlertType) {
			SQLiteDatabase db = null;
			try {
				db = this.getWritableDatabase();

				ContentValues values = new ContentValues();
				values.put(KEY_STATUS, status); 
				values.put(KEY_TIME, time);
				values.put(KEY_ALERTTYPE, AlertType);
				String selectQuery="SELECT * FROM "+ TABLE_DATA+" WHERE "+KEY_ALERT_ID+" = (SELECT MAX("+KEY_ALERT_ID+") FROM "+TABLE_DATA+" where "+KEY_ALERTTYPE+"='"+AlertType+"')";
				
				boolean exists = false;
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					do {
						String stausFromDatabase = cursor.getString(cursor.getColumnIndex("status"));
						
						
						if(status.equals(stausFromDatabase)){
							exists = true;
						
						}
						
					} while (cursor.moveToNext());
				}
				
				if(!exists) {
					db.insert(TABLE_DATA, null, values);
				}
				
			} catch (Exception e) {
				
			} finally {
				if(db != null) {
					db.close();
				}
			}
		}
		
		
		public String getData() {
			SQLiteDatabase db = null;
			try {
				String selectQuery = "SELECT  * FROM " + TABLE_DATA;

				db = this.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);

				String text = "";
				if (cursor.moveToFirst()) {
					do {
						String staus = cursor.getString(1);
						String time = cursor.getString(2);	
						String alertType = cursor.getString(3);					
						text = text+(alertType+" "+staus+" "+time+" \n"); 				
					} while (cursor.moveToNext());
				}
			
				return text;
			} catch (Exception e) {
				return "";
			} finally {
				if(db != null) {
					db.close();
				}
			}
		}
		
		
		public void deleteData() {
			SQLiteDatabase db = null;
			try {
				db = this.getWritableDatabase();
				db.execSQL("DELETE FROM "+TABLE_DATA);
			} catch (SQLException e) {
				
			} finally {
				if(db != null) {
					db.close();
				}
			}
		}


		
}
