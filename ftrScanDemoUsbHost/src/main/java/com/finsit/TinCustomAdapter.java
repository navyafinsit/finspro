package com.finsit;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.finsit.R;

import java.util.ArrayList;

public class TinCustomAdapter extends BaseAdapter {
    private static ArrayList<TinDetailsBean> searchArrayList;

    private LayoutInflater mInflater;

    Context context;

    public TinCustomAdapter(Context context, ArrayList<TinDetailsBean> results) {
        searchArrayList = results;
        mInflater = LayoutInflater.from(context);
        this.context=context;

    }



    public int getCount() {
        return searchArrayList.size();
    }

    public Object getItem(int position) {
        return searchArrayList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_view_list, null);
            holder = new ViewHolder();
            holder.type = (TextView) convertView.findViewById(R.id.type);
            holder.tinNumber = (TextView) convertView.findViewById(R.id.tinNumber);
            holder.district = (TextView) convertView.findViewById(R.id.district);
            holder.state=(TextView)convertView.findViewById(R.id.state);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tinNumber.setText(searchArrayList.get(position).getTinNumber());

        if(searchArrayList.get(position).getType().equalsIgnoreCase("A"))
        {
            holder.type.setText("ACCUSED");
        }else       if(searchArrayList.get(position).getType().equalsIgnoreCase("C"))
        {
            holder.type.setText("CONVICTED");
        }else       if(searchArrayList.get(position).getType().equalsIgnoreCase("S"))
        {
            holder.type.setText("SUSPECT");
        }

        holder.district.setText(searchArrayList.get(position).getDistrict());
        holder.state.setText(searchArrayList.get(position).getState());


        //stting the colour to the rows..
       String state= searchArrayList.get(position).getState();
        if (state.equalsIgnoreCase("AP")) {
            convertView.setBackgroundColor(Color.parseColor("#F2F7BC"));
        } else if (state.equalsIgnoreCase("TS")) {
            convertView.setBackgroundColor(Color.parseColor("#F2BBD2"));
        }


        return convertView;
    }

    static class ViewHolder {
        TextView type;
        TextView tinNumber;
        TextView district;
        TextView state;


    }


}