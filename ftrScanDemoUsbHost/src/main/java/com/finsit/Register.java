package com.finsit;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class Register extends Activity {




        CircleImageView circleImageViewProfile;


        ImageView addImageIcon;

        EditText editTextFirstName;
        EditText editTextMobileNumber;
        EditText editTextAddress;

        Button regsub;
        private String picturePath;
        private Uri pickupedImagePath;
        boolean imageIssue=false;
        SqliteHelper sqliteHelper;
        Bitmap fingerImage;

@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_reg);
        sqliteHelper = new SqliteHelper(this);
        regsub=(Button)findViewById(R.id.regsub);

        addImageIcon=(ImageView)findViewById(R.id.addProfileImage) ;
        circleImageViewProfile=(CircleImageView)findViewById(R.id.circleImageViewProfile);
        editTextFirstName=(EditText)findViewById(R.id.firstName);
        editTextMobileNumber=(EditText)findViewById(R.id.mobilenumber);
        editTextAddress=(EditText)findViewById(R.id.address);

       // fingerImage=getIntent().getParcelableExtra("fingerImage");
   /* FtrScanDemoUsbHostActivity.mFingerImage.getDrawable()
    byte[] fingerByteArray=userModel.getFingerImage();
    final Bitmap fingerBitmap = BitmapFactory.decodeByteArray(fingerByteArray, 0, fingerByteArray.length);

        fingerImage=FtrScanDemoUsbHostActivity.imgBitMap;
*/
        addImageIcon.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              selectImage();
             // validateFields();
          }
        });


        regsub.setOnClickListener(new View.OnClickListener() {

                public void onClick(View arg0)
                {
                    try{
                       if(validateFields())
                       {
                           Intent i=new Intent(Register.this, UsersListViewActivity.class);
                           startActivity(i);
                           finish();
                       }

                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }


                }


        });





}
       /* protected void showSnackBar(String str, View view){
                Snackbar snackbar = Snackbar
                        .make(view, str, Snackbar.LENGTH_LONG);
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
        }*/

        boolean validateFields (){
              //  String firstName = editTextFirstName.getText().toString().trim();
             //   String lastName = editTextLastName.getText().toString().trim();
            //    String mobileNumber = editTextMobileNumber.getText().toString().trim();


            //String profile = circleImageViewProfile.getDrawable().toString().trim();

                if (circleImageViewProfile.getDrawable()==null ) {
                      //  showSnackBar("Please upload Profile Picture.", circleImageViewProfile);
                    Toast.makeText(this, "Please enter Profile Picture.",Toast.LENGTH_LONG).show();
                        return false;
                }




               if (editTextFirstName.getText()==null || editTextFirstName.getText().toString().length()==0) {
                       Toast.makeText(this, "Please enter first name.",Toast.LENGTH_LONG).show();

                        return false ;
                }

            if (editTextMobileNumber.getText()==null || editTextMobileNumber.getText().toString().length()==0) {
                Toast.makeText(this, "Please enter MobileNumber.",Toast.LENGTH_LONG).show();
                //showSnackBar("Please enter MobileNumber.", editTextMobileNumber);

                return false ;
            }

            if (editTextAddress.getText()==null || editTextAddress.getText().toString().length()==0) {
                //showSnackBar("Please enter address.", editTextAddress);
                Toast.makeText(this, "Please enter Address.",Toast.LENGTH_LONG).show();
                return false;
            }
            Drawable d = circleImageViewProfile.getDrawable();
            Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
           // bmp.recycle();






            Drawable fd = FtrScanDemoUsbHostActivity.mFingerImage.getDrawable();
            Bitmap fingerbitmap = ((BitmapDrawable)fd).getBitmap();

            fingerImage=fingerbitmap;
            ByteArrayOutputStream fingerStream = new ByteArrayOutputStream();
            fingerImage.compress(Bitmap.CompressFormat.PNG, 50, fingerStream);
            byte[] fingerByteArray = fingerStream.toByteArray();

               sqliteHelper.addUser(new UserModel(editTextFirstName.getText().toString(),
                                                  editTextMobileNumber.getText().toString(),
                                                  editTextAddress.getText().toString(),
                                                  byteArray,
                                                  fingerByteArray));


            return true;


      /*      Snackbar.make(regsub, "User created successfully! Please Login ", Snackbar.LENGTH_LONG).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, Snackbar.LENGTH_LONG);*/
        }

    private void selectImage() {
      /*  final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(Register.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo"))
                {

                }
                else if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();*/

/*
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(getExternalStorageDirectory(), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, 1);
*/


        if (ActivityCompat.checkSelfPermission(Register.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            ActivityCompat.requestPermissions(Register.this, new String[]{Manifest.permission.CAMERA}, 0);
            return;
        }
        else
        {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, 1);
        }


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == Activity.RESULT_OK)
        {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            circleImageViewProfile.setImageBitmap(photo);
        }
    }


}