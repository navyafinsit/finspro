package com.finsit;
  
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;


public class FinsOptions extends Activity {
	
	
	
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fins_options);
		
		final ImageButton fingerOption=(ImageButton)findViewById(R.id.qrscan);
		
		fingerOption.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(FinsOptions.this,FtrScanDemoUsbHostActivity.class);
				startActivity(i);
                finish();
				
			}
		});

		final ImageButton faceOption=(ImageButton)findViewById(R.id.punch);

		faceOption.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i=new Intent(FinsOptions.this,FaceScanActivity.class);
				startActivity(i);
				finish();

			}
		});


		final ImageButton uploadScreen=(ImageButton)findViewById(R.id.offline);

		uploadScreen.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				SharedPreferences data = getSharedPreferences("datastore", Context.MODE_PRIVATE);
				String deviceId=data.getString("deviceid",null);
				Intent i=new Intent(FinsOptions.this,StartPage.class);
				i.putExtra("deviceId",deviceId);
				startActivity(i);
				finish();

			}
		});



		final ImageButton offmode=(ImageButton)findViewById(R.id.off);

		offmode.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent i=new Intent(FinsOptions.this, UsersListViewActivity.class);
				startActivity(i);
				finish();

			}
		});




}
	/*public void openScannerScreen()
	{

		Toast.makeText(this,"hello",Toast.LENGTH_LONG).show();
	}*/
	


	
		
	
		
		
		
		
	}
	
	
	
	

