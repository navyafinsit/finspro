package com.finsit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Date;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.finsit.R;

public class ShowCirminalInfo extends Activity {
	Button backButton;
	Button showCrimeLocation;
    Button screenshotButton;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.more_information);
        backButton=(Button) findViewById(R.id.backButton);
        showCrimeLocation=(Button) findViewById(R.id.showCrimeLocation);
        screenshotButton=(Button) findViewById(R.id.screenshot);
        screenshotButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeScreenshot();
            }
        });
        //added by dinesh
       /* WindowManager.LayoutParams params = getWindow().getAttributes();
       // params.x = -20;
       // params.height = getWindow().get
       // params.width = 550;
        // params.y = -10;
       // params.gravity=50;


        this.getWindow().setAttributes(params);
*/

        TinDetailsBean fullObject=(TinDetailsBean)getIntent().getSerializableExtra("info");
        //End of added code by dinesh


        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        Bundle bundle=getIntent().getExtras();
        //String urlAddress =bundle.getString("templateURL");
        String urlAddress =fullObject.getTemplateURL();
        //final String criminalName=bundle.getString("criminalName");
        final String criminalName=fullObject.getName();
       /* String address=bundle.getString("address");*/
        String address=fullObject.getAddress();
       /* String phoneNo=bundle.getString("phoneNo");*/
        String phoneNo=fullObject.getFathersName();
        final String latitude=bundle.getString("latitude");
        final String longtitude=bundle.getString("longtitude");
        String personCategory=bundle.getString("personCategory");
        //String webImageURL=bundle.getString("webImage");
        String webImageURL=fullObject.getWebUrl();
        ImageView iv = (ImageView)findViewById(R.id.criminalImage);
        ImageView webImage = (ImageView)findViewById(R.id.webImage);
        TextView criminalView=(TextView) findViewById(R.id.criminalName);
        TextView addressView=(TextView) findViewById(R.id.address);
        TextView phoneNumber=(TextView) findViewById(R.id.district);
        TextView latitudeText=(TextView) findViewById(R.id.latitudeText);
        TextView longtitudeText=(TextView) findViewById(R.id.longtitudeText);
        TextView personCategoryText=(TextView) findViewById(R.id.personCategory);
        ImageButton backButton=(ImageButton)findViewById(R.id.backButtonClose);
        System.out.println("*-*-*-* Criminal detials:"+criminalName+" and "+urlAddress);
        criminalView.setText(criminalName);
        addressView.setText(address);
        phoneNumber.setText(phoneNo);
        latitudeText.setText(latitude);
        longtitudeText.setText(longtitude);
       /* if(personCategory.equalsIgnoreCase("S")){
        	personCategoryText.setText("CATEGORY : SUSPECT");
        }else if(personCategory.equalsIgnoreCase("C")){
        	personCategoryText.setText("CATEGORY : CRIMINAL");
        }else{
        	personCategoryText.setText("CATEGORY : None");
        }*/
        
		try {
			URL newurl = new URL(urlAddress); 
			Bitmap criminalImage = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());
			iv.setImageBitmap(criminalImage);
			URL webUploadedImageURL = new URL(webImageURL); 
			Bitmap webUploadedImage = BitmapFactory.decodeStream(webUploadedImageURL.openConnection() .getInputStream());
			webImage.setImageBitmap(webUploadedImage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		backButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent backIntent=new Intent(getApplicationContext(),CrimeReportActivity.class);
				startActivity(backIntent);
				finish();
			}
		});
		showCrimeLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent passLatLang=new Intent(getApplicationContext(),MainActivity.class);
				passLatLang.putExtra("latitude", latitude);
				passLatLang.putExtra("longtitude", longtitude);
				passLatLang.putExtra("name",criminalName);
				startActivity(passLatLang);
			}
		});

        backButton.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {

                                              Intent i = new Intent();

                                              setResult(RESULT_CANCELED, i);

                                              finish();
                                          }
                                      });

/*        Bitmap bmp;
        try {
			bmp = BitmapFactory.decodeStream(new java.net.URL(urlAddress).openStream());
			iv.setImageBitmap(bmp);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
        
/*        URL url;
		try {
			  url = new URL(urlAddress);
		      InputStream content = (InputStream)url.getContent();
		      Drawable d = Drawable.createFromStream(content , "src"); 
		      iv.setImageDrawable(d);
		      criminalView.setText(criminalName);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent();

        setResult(RESULT_CANCELED, i);

        finish();
    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }


    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }


}