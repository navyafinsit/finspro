package com.finsit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.finsit.R;

import org.json.JSONArray;
import org.json.JSONObject;


public class TestgpsActivity extends Activity {
	ProgressDialog serverProgress;
	private String phoneno = ""; // in Meters
	private long time = 0; // in Milliseconds
	private String textData;
	private String deviceid = null;
	private String ipid = null;
	long manualPollUpdatesTime = 300000;
	private String outputData = "";
	private TextView errorText, menuTextView;
	private Button saveBtn, cancelBtn;
	private Button updateBtn;
	boolean isGPSEnabled = false;
	Button reconnectButton, logEmailButton, deleteButton;
	// flag for network status
	boolean isNetworkEnabled = false;
	private static final int START_GPS = 1;
	private static final int STOP_GPS = 2;
	private static final int MODIFY_DATA = 3;
	String fileName = "errordata.txt";
	File root = Environment.getExternalStorageDirectory();
	boolean isValidated = false;
	SharedPreferences data;
	boolean availableFlag = false;
	LocationManager locationManager;
	String phoneNumber = "";
	String configTime = "";
	String configDevice = "";
	String configIP = "";
	AlertDialog ad = null;
	boolean finish = false;


	final class DeviceConfigureTask extends AsyncTask<String, Void, String> {


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			serverProgress.show();
		}

		@Override
		protected String doInBackground(String... urls) {
			String output = null;
			for (String url : urls) {
				output = getOutputFromUrl(url);

			}
			return output;
		}

		private String getOutputFromUrl(String url) {
			String output = "";

			try {
				String urlStr = url.toString().replace(" ", "%20");
				URL urlData = new URL(urlStr);
				URLConnection yc = urlData.openConnection();
				yc.setConnectTimeout(95000);

				BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
				String inputResult = "";
				String inputLine = "";
				while ((inputLine = in.readLine()) != null) {
					inputResult = inputResult + inputLine;

				}


				in.close();


				output = inputResult;

			} catch (Exception e) {
				output = "";
				output = "INTERNETPROBLEM";


			}

			return output;
		}

		@Override
		protected void onPostExecute(String output) {

			try {
				serverProgress.cancel();


				if (output.contains("FALSE")) {
					Toast.makeText(getApplicationContext(), "Device Id already exists", Toast.LENGTH_LONG).show();
					//alertboxToConfigure("DUPLICATE DEVICE ID",  "Device Id already exists");
					//Toast.
					availableFlag = false;
				} else if (output.contains("TRUE")) {
					availableFlag = true;
				} else if (output.contains("NONEXIST")) {
					//alertboxToConfigure("WRONG DEVICE ID",  "Device Id not found in Database");
					Toast.makeText(getApplicationContext(), "Device Id not found in Database", Toast.LENGTH_LONG).show();
					availableFlag = false;
				}

				if (availableFlag) {

					if (phoneNumber.equals("")) {
						phoneNumber = "30";
					}

					if (configTime.equals("")) {
						configTime = "0";
					}
					if (configIP.equals("")) {
						configIP = IPInfo.IPADDRESS;
					}


					ad.cancel();

					SharedPreferences data = getSharedPreferences("datastore", Context.MODE_PRIVATE);
					SharedPreferences.Editor editor = data.edit();

					editor.putString("deviceid", configDevice);
					editor.putString("ipid", configIP);
					editor.putString("phoneno", phoneNumber);
					editor.putLong("time", Long.parseLong(configTime));
					editor.putLong("manualtimer", 300000);
					editor.putString("pass", "demo");

					editor.commit();
					alertboxToConfigure("CONFIGURATION SUCCESS", "Device Configured Successfully");


					try {
						DatabaseHandler dbHandler = new DatabaseHandler(TestgpsActivity.this);
					} catch (Exception e) {
						e.printStackTrace();
					}


					// newly added code 5 jan
					File file1 = new File(
							Environment
									.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
							configDevice);
					File file2 = new File(
							Environment
									.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
							configDevice + "-RS");
					/*	File file3 = new File(
								Environment
										.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
										configDevice+"-ROWDY");
						File file4 = new File(
								Environment
										.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
						
										
										configDevice+"-SUSPECT");*/

					if (!file1.mkdirs()) {
						file1.mkdir();
					}
					if (!file2.mkdirs()) {
						file2.mkdir();
					}

						/*if(!file3.mkdirs()) {
						    file3.mkdir();
						}
						if(!file4.mkdirs()) {
							file4.mkdir();
						}	*/

				}


			} catch (Exception e) {

			}


		}
	}

	boolean isMailSent = false;

	boolean enableGPS = false;


	final class DownloadSettingsTask extends AsyncTask<String, Void, String> {


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			serverProgress.show();
		}

		@Override
		protected String doInBackground(String... urls) {
			String output = null;
			for (String url : urls) {
				output = getOutputFromUrl(url);

			}
			return output;
		}


		private String getOutputFromUrl(String url) {
			String output = "";

			try {
				String urlStr = url.toString().replace(" ", "%20");
				URL urlData = new URL(urlStr);
				URLConnection yc = urlData.openConnection();
				yc.setConnectTimeout(95000);

				BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
				String inputResult = "";
				String inputLine = "";
				while ((inputLine = in.readLine()) != null) {
					inputResult = inputResult + inputLine;

				}


				in.close();


				output = inputResult;

			} catch (Exception e) {
				output = "";
				output = "INTERNETPROBLEM";


			}

			return output;
		}

		@Override
		protected void onPostExecute(String output) {

			try {
				serverProgress.cancel();

				if (output != null) {

					//check GPS connectivity
					isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

					JSONArray jArray = new JSONArray(output);

					String messageFromServer = "";
					boolean enabled = false;
					boolean serverDown = false;
					int counter = 0;
					for (int i = 0; i < jArray.length(); i++) {

						JSONObject obj = jArray.getJSONObject(i);
						enableGPS = obj.getBoolean("enableGPS");
						messageFromServer = obj.getString("messageFromServer");
						enabled = obj.getBoolean("enabled");
						CustomListView.showTinInformation = obj.getBoolean("showTinDetails");
						serverDown = obj.getBoolean("serverDown");
						counter = obj.getInt("counter");
					}
					//alertboxToLogin("Scans Remaining today", "Remaining"+ (10-counter));

					if (counter == 10) {
						alertboxToLogin("Counter exceeded", "Counter limit is reached");
						finish = true;
					}


					if (serverDown) {
						alertboxToLogin("Server is under maintainence", "Please try again later");
						finish = true;
						//finish();
					}

					if (!enabled) {
						alertboxToLogin("Your Device is Disabled", "Device is Disabled!");
						finish = true;
						//finish();
					}


					if (enableGPS && !isGPSEnabled) {
						showSettingsAlert();
						//finish();
						finish = true;
					}

					if (!finish) {
						/*Intent i=new Intent(TestgpsActivity.this,FtrScanDemoUsbHostActivity.class);
						startActivity(i);
						*/

						Intent i = new Intent(TestgpsActivity.this, FinsOptions.class);
						startActivity(i);


					}







				/*	for (int i = 0; i < jArray.length(); i++) {
				 			JSONObject obj = jArray.getJSONObject(i);
								enableGPS=obj.getBoolean("enableGPS");
								break;
							case 1:
								// for enabling somet hing else..


						}


					}*/

				} //end of if

			} catch (Exception e) {
				e.printStackTrace();
			} //end of catch


		} // end of method
	} // end of class


	final class MailTask extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			isMailSent = false;
			serverProgress.setMessage("Emailing Log Report...");
			serverProgress.show();
		}

		@Override
		protected String doInBackground(String... urls) {
			try {
				DatabaseHandler dbHandler = new DatabaseHandler(TestgpsActivity.this);
				SharedPreferences data = getSharedPreferences("datastore", Context.MODE_PRIVATE);
				String deviceId = data.getString("deviceid", null);
				String text = dbHandler.getData();
				//System.out.println(text.length());
				if (text.length() > 0) {
					GMailSender sender = new GMailSender("quadriviumtracking@gmail.com", "quad@123");
					sender.sendMail("GPS AND INTERNET REPORTS OF " + deviceId,
							text,
							"quadriviumtracking@gmail.com", "quadriviumbeats@gmail.com");
					dbHandler.deleteData();
					return "SUCCESS";

				} else {
					return "NOLOG";
				}

			} catch (Exception e) {

				return "EXCEPTION";
			}

		}


		@Override
		protected void onPostExecute(String output) {

			try {

				serverProgress.cancel();

				if ("SUCCESS".equals(output)) {

				}

				//alertboxToConfigureEmail("EMAIL STATUS",  "Log Reports mailed");


				//alertboxToConfigureEmail("EMAIL STATUS",  "No Log Reports exist");
			} catch (Exception e) {

			}

		}
	}


	public void setMobileDataEnabled(Context context, boolean enabled) throws Exception {
		final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		final Class conmanClass = Class.forName(conman.getClass().getName());
		final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
		iConnectivityManagerField.setAccessible(true);
		final Object iConnectivityManager = iConnectivityManagerField.get(conman);
		final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
		final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
		setMobileDataEnabledMethod.setAccessible(true);

		setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
	}


	public void alert() {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TestgpsActivity.this);

		// set title
		alertDialogBuilder.setTitle("Delete Backup Data");

		// set dialog message
		alertDialogBuilder
				.setMessage("You are about to delete backup data. If any backup data persists it gets deleted.")
				.setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						try {
							DatabaseHandler dbHandler = new DatabaseHandler(TestgpsActivity.this);
							dbHandler.delete();
							Toast.makeText(getApplicationContext(), "Backup data deleted successfully", Toast.LENGTH_LONG).show();
						} catch (Exception e) {
							e.printStackTrace();
							Toast.makeText(getApplicationContext(), "Backup data not sent", Toast.LENGTH_LONG).show();
						}

					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it

		alertDialog.show();

	}


	public void alertEmail() {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TestgpsActivity.this);

		// set title
		alertDialogBuilder.setTitle("Email Log Report");

		// set dialog message
		alertDialogBuilder
				.setMessage("You are about to mail GPS and Internet Logs. Do you really want to continue.")
				.setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						try {

							ConnectionDetector connectionDetector = new ConnectionDetector(TestgpsActivity.this);

							if (connectionDetector.isConnectingToInternet()) {
								MailTask mailTask = new MailTask();
								mailTask.execute("MAIL");

							} else {
								Toast.makeText(getApplicationContext(), "Please Enable Internet !", Toast.LENGTH_LONG).show();
								dialog.cancel();
							}

						} catch (Exception e) {

						}

					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();
					}
				});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it

		alertDialog.show();

	}


	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.validation);
		menuTextView = (TextView) findViewById(R.id.menutext);
		reconnectButton = (Button) findViewById(R.id.reconnect);
		logEmailButton = (Button) findViewById(R.id.enable);
		deleteButton = (Button) findViewById(R.id.deletebackup);

		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}


		reconnectButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String iptext = "";
				String phonenoText = "";
				String timetext = "";

				SharedPreferences data = getSharedPreferences("datastore", Context.MODE_PRIVATE);

				SharedPreferences.Editor editor = data.edit();

				if (!iptext.equals("")) {
					editor.putString("ipid", iptext);
				}

				if (!phonenoText.equals("")) {
					editor.putString("phoneno", phonenoText);
				}

				if (!timetext.equals("")) {
					editor.putLong("time", Long.parseLong(timetext));
				}


				editor.commit();
				stopService(new Intent(TestgpsActivity.this, GPSService.class));
				data = getSharedPreferences("datastore", Context.MODE_PRIVATE);
				deviceid = data.getString("deviceid", null);
				ipid = data.getString("ipid", IPInfo.IPADDRESS);
				phoneno = data.getString("phoneno", "");
				time = data.getLong("time", 0);
				manualPollUpdatesTime = data.getLong("manualtimer", 300000);

				isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);


				if (!isGPSEnabled) {
					showSettingsAlert();
				} else {


					Intent gpsService = new Intent(TestgpsActivity.this, GPSService.class);

					Bundle b = new Bundle();
					b.putString("deviceid", deviceid);
					b.putString("ipid", ipid);
					b.putString("phoneno", phoneno);
					b.putLong("time", time);
					b.putLong("manualtimer", manualPollUpdatesTime);
					gpsService.putExtras(b);
					startService(gpsService);
					Toast.makeText(getApplicationContext(), "You Are Being Tracked", Toast.LENGTH_LONG).show();
					finish();

				}


			}
		});

		logEmailButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					alertEmail();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}
		});


		deleteButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				try {
					alert();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}
		});


		serverProgress = new ProgressDialog(TestgpsActivity.this);
		serverProgress.setMessage("Configuring Device. Please wait");
		serverProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		SharedPreferences dataPreference = getSharedPreferences("datastore", Context.MODE_PRIVATE);
		textData = dataPreference.getString("deviceid", null);

		phoneNumber = "";
		configTime = "";
		configDevice = "";
		configIP = "";

		if (textData == null) {
			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			final View layout = inflater.inflate(R.layout.entrydialog, (ViewGroup) findViewById(R.id.layout_root));

			final AlertDialog.Builder adb = new AlertDialog.Builder(TestgpsActivity.this);
			adb.setTitle("Configuration Dialog");
			adb.setView(layout);
			adb.setCancelable(false);
			ad = adb.create();
			ad.show();
			
			/*deviceEditText = (EditText) layout.findViewById(R.id.deviceid);
			ipEditText = (EditText) layout.findViewById(R.id.ipid);*/
			saveBtn = (Button) layout.findViewById(R.id.save);
			cancelBtn = (Button) layout.findViewById(R.id.cancel);
			//saveBtn.setEnabled(false);
			errorText = (TextView) layout.findViewById(R.id.errortext);


			saveBtn.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub
					ConnectionDetector connectionDetector = new ConnectionDetector(TestgpsActivity.this);
					if (connectionDetector.isConnectingToInternet()) {
						phoneNumber = ((EditText) layout.findViewById(R.id.phoneno)).getText().toString().trim();
						configTime = ((EditText) layout.findViewById(R.id.time)).getText().toString().trim();
						configDevice = ((EditText) layout.findViewById(R.id.deviceid)).getText().toString().trim();
						configIP = ((EditText) layout.findViewById(R.id.ipid)).getText().toString().trim();
						if (configIP.equals("")) {
							configIP = IPInfo.IPADDRESS;
						}

						if (configDevice.equals("")) {
							errorText.setText("Please Provide Device Id");
						} else {
							// String urlStrDevice = "http://"+configIP+"/vehicleservice/device?devicename="+configDevice;
							//To get the IMEI number of the phone.
							TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
							if (ActivityCompat.checkSelfPermission(TestgpsActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
								// TODO: Consider calling
								//    ActivityCompat#requestPermissions
								// here to request the missing permissions, and then overriding
								//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
								//                                          int[] grantResults)
								// to handle the case where the user grants the permission. See the documentation
								// for ActivityCompat#requestPermissions for more details.

								ActivityCompat.requestPermissions(TestgpsActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
								return;
							}
							String IMEINumber = mngr.getDeviceId();

							   // String urlStrDevice = "http://"+configIP+"/vehicleservice/device?devicename="+configDevice;
                           // String urlStrDevice = "http://"+configIP+"/vehicleservice/device?devicename="+configDevice+"&imei="+IMEINumber;
							String urlStrDevice = "http://"+configIP+"/finsprows/device?devicename="+configDevice+"&imei="+IMEINumber;
                            try{
							         urlStrDevice.replaceAll(" ", "%20");
									 DeviceConfigureTask deviceConfigureTask = new   DeviceConfigureTask();
							         
							         
							         deviceConfigureTask .execute(urlStrDevice);
								  } catch (Exception e) {
									  Toast.makeText(getApplicationContext(), "Problem with Internet Connection", Toast.LENGTH_LONG).show();
								   }
							      
							}
						
					
				}
					else
					{
						alertboxToLogin("Internet Connectivity Failed", "Make sure your device has Internet Connection");
					}
				
				}
			});
			
			cancelBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ad.cancel();
					finish();
					
				}
			});
		
		    
		} else {
			try {
				
				
				data = getSharedPreferences("datastore", Context.MODE_PRIVATE);
				
				final String password = data.getString("pass", null);
				String deviceIdentity = data.getString("deviceid", null);
				AlertDialog.Builder alert = new AlertDialog.Builder(this);

				alert.setTitle("Authentication Required Now");
				alert.setMessage("Password for "+deviceIdentity);

				// Set an EditText view to get user input 
				final EditText input = new EditText(this);
				input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
				alert.setView(input);
                alert.setCancelable(false);
				input.setText("demo");
				alert.setPositiveButton("Login", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
				  /*  reconnectButton.setVisibility(View.VISIBLE);
				    logEmailButton.setVisibility(View.VISIBLE);
				    deleteButton.setVisibility(View.VISIBLE);
				    menuTextView.setVisibility(View.VISIBLE);*/

					String value = input.getText().toString();
					if(value != null) {
						value = value.trim();
						
						if(value.equals(password)) {
							deviceid = data.getString("deviceid", null);
							ipid = data.getString("ipid", IPInfo.IPADDRESS);
							phoneno = data.getString("phoneno","");
							time = data.getLong("time",0);
							manualPollUpdatesTime = data.getLong("manualtimer", 300000);

							if(configIP.equals("")) {
								configIP = IPInfo.IPADDRESS;
							}

							SharedPreferences.Editor editor = data.edit();
							editor.putString("deviceid", deviceid);
							editor.putString("ipid", ipid);
							editor.putString("phoneno", phoneno);
							editor.putLong("time", time);
							editor.putLong("manualtimer", manualPollUpdatesTime);
							editor.commit();

							//check internet connectivity dinesh

							ConnectionDetector connectionDetector=new ConnectionDetector(TestgpsActivity.this);
							if(!connectionDetector.isConnectingToInternet()) {
								alertboxToLogin("Internet Connectivity Failed", "Check Internet Connection." +"\n"+"\n"+
										"If you Want to Offline Mode");
								//finish();
							}

							//download settings from server
							String urlStrDevice = "http://"+configIP+"/finsprows/DeviceSettings?deviceID="+deviceid;
							System.out.println("Dinesh:"+urlStrDevice);
							try{
								urlStrDevice.replaceAll(" ", "%20");
								DownloadSettingsTask downloadtask = new   DownloadSettingsTask();
								downloadtask.execute(urlStrDevice);

								} catch (Exception e) {
								Toast.makeText(getApplicationContext(), "Problem with Internet Connection", Toast.LENGTH_LONG).show();
							}



						}else {
							Toast.makeText(getApplicationContext(),"Authentication Failed",Toast.LENGTH_LONG).show();
							alertboxToLogin("Internet Connectivity Failed", "Authentication Failed");
							finish();
						}
					}
				 
				  }
				});
				alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						finish();
					  
			    }
					});

				alert.show();
				
				
			}catch (Exception e) {
				
				
				try {
					FileOutputStream f = new FileOutputStream(new File(root, fileName),true);
					StackTraceElement s[] = e.getStackTrace();
					Calendar c = Calendar.getInstance(); 
					String writeCard ="*********"+ c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND)+"  ";
					for(int i=0;i<s.length;i++) {
						String errorData = s[i].toString();
						if(errorData.contains("com.quad.vehicle")) {
							writeCard = writeCard + errorData;
							break;
						}
					}
					writeCard = writeCard+"  "+e.getClass()+":"+e.getMessage();
					
					byte b[] = new byte[writeCard.length()];
					b = writeCard.getBytes();
					f.write(b);		
					
				} catch (Exception e1) { 
					
				}
			}
			
		}
	
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		MenuItem startGPS = menu.add(0, START_GPS, 1, "Start Tracking");
		MenuItem stopGPS = menu.add(0,STOP_GPS,2,"Stop Tracking");
		MenuItem modifyData = menu.add(0,MODIFY_DATA,3,"Edit");
		
		if(deviceid == null) {
			startGPS.setEnabled(false);
			stopGPS.setEnabled(false);
		} else {
			startGPS.setEnabled(true);
			stopGPS.setEnabled(true);
		}
		
		return super.onCreateOptionsMenu(menu);
	}
	
	
	public void alertboxToLogin(String title, String msg) {
		if (msg != null) {
			 new AlertDialog.Builder(this)
		      .setMessage(msg)
		      .setTitle(title)
		      .setCancelable(true)
	 .setPositiveButton(R.string.save,
							 new DialogInterface.OnClickListener() {
								 public void onClick(DialogInterface dialog, int whichButton) {
									 Intent i=new Intent(TestgpsActivity.this,FtrScanDemoUsbHostActivity.class);
									 i.putExtra("OFFLINE_MODE","YES");
									 startActivity(i);
									 finish();
								 }
							 })
					 .setNegativeButton(R.string.notsave,
							 new DialogInterface.OnClickListener() {
								 public void onClick(DialogInterface dialog, int whichButton){
									 finish();
								 }


							 })
					 .show();
		}
	}	
	
	
	
	public void alertboxToConfigure(String title, final String msg) {
		if (msg != null) {
			 new AlertDialog.Builder(this)
		      .setMessage(msg)
		      .setTitle(title)
		      .setCancelable(true)
		      .setNeutralButton(android.R.string.ok,
		    		  new DialogInterface.OnClickListener() {
		    	  public void onClick(DialogInterface dialog, int whichButton){
		    		  if(msg.contains("Device Configured Successfully"))
		    		  {
		    		  Intent i=new Intent(TestgpsActivity.this,FtrScanDemoUsbHostActivity.class);
		    		  startActivity(i);
		               finish();
		    		  }
		    		/*  else
		    		  {
		    			  Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
		    			  return;
		    		  }*/
		         }
		         }).show();
		}
	}	
	
	
	public void alertboxToConfigureEmail(String title, String msg) {
		if (msg != null) {
			 new AlertDialog.Builder(this)
		      .setMessage(msg)
		      .setTitle(title)
		      .setCancelable(true)
		      .setNeutralButton(android.R.string.ok,
		    		  new DialogInterface.OnClickListener() {
		    	  public void onClick(DialogInterface dialog, int whichButton){
		               	
		         }
		         }).show();
		}
	}	

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		
		
		return super.onPrepareOptionsMenu(menu);
	}



	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		try {
		 int selectedItem = item.getItemId();
		 
		 switch(selectedItem) {
		 
		 	case START_GPS:  	
		 		
		 		                isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

								
								if (!isGPSEnabled) {
									showSettingsAlert();
								} else {
								
			 		                 Intent gpsService = new Intent(TestgpsActivity.this,GPSService.class);
				 					 Bundle b = new Bundle();
				 					 b.putString("deviceid", deviceid);
				 					 b.putString("ipid", ipid);
				 					 b.putString("phoneno", phoneno);
				 					 b.putLong("time", time);
				 					 b.putLong("manualtimer", manualPollUpdatesTime);
				 					 gpsService.putExtras(b);
				 					 startService(gpsService);
				 					 Toast.makeText(getApplicationContext(), "You Are Being Tracked", Toast.LENGTH_LONG).show();
				 					 finish();
								}
		 		                
			 					 break;
			 
		 	case STOP_GPS:   	 
		 		                 
		 		                 stopService(new Intent(TestgpsActivity.this,GPSService.class));
		 		                 Toast.makeText(getApplicationContext(), "Tracking Stopped", Toast.LENGTH_LONG).show(); 
		 						 finish();
		 					 	 break;
		 					 
		 					 	 
		 	case MODIFY_DATA: 	 try {
		 						 LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
								 final View layout = inflater.inflate(R.layout.editdialog,(ViewGroup) findViewById(R.id.layout_editroot));
								
								 final AlertDialog.Builder adb = new AlertDialog.Builder(TestgpsActivity.this);
								 adb.setTitle("Update Configuration Data");
								 adb.setView(layout);
								 adb.setCancelable(false);
								 final AlertDialog ad = adb.create();
								 ad.show();
								
								 updateBtn = (Button) layout.findViewById(R.id.update);
								 cancelBtn=(Button)   layout.findViewById(R.id.cancel);
								 cancelBtn.setOnClickListener(new View.OnClickListener() {
									
									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
										ad.cancel();
										finish();
										
									}
								});
								 updateBtn.setOnClickListener(new View.OnClickListener() {
									
									public void onClick(View v) {
										// TODO Auto-generated method stub
										
										String iptext = ((EditText)layout.findViewById(R.id.ipedit)).getText().toString().trim();
										String phoneNoText = ((EditText)layout.findViewById(R.id.distanceedit)).getText().toString().trim();
										String timetext = ((EditText)layout.findViewById(R.id.timeedit)).getText().toString().trim();
										
										SharedPreferences data = getSharedPreferences("datastore", Context.MODE_PRIVATE);
										
										SharedPreferences.Editor editor = data.edit();
										
										if(!iptext.equals("")) {
											editor.putString("ipid", iptext);
										}
										
										if(!phoneNoText.equals("")) {
											editor.putString("phoneno", phoneNoText);
										}
										
										if(!timetext.equals("")) {
											editor.putLong("time", Long.parseLong(timetext));
										}
										
										
										editor.commit();
										stopService(new Intent(TestgpsActivity.this,GPSService.class));
										data = getSharedPreferences("datastore", Context.MODE_PRIVATE);
										deviceid = data.getString("deviceid", null);
										ipid = data.getString("ipid", IPInfo.IPADDRESS);
										phoneno = data.getString("phoneno","");
										time = data.getLong("time",0);
										manualPollUpdatesTime = data.getLong("manualtimer",300000);
										
										
			                            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

											
											if (!isGPSEnabled) {
												showSettingsAlert();
											} else {
											
										
										Intent gpsService = new Intent(TestgpsActivity.this,GPSService.class);
					 					 
										Bundle b = new Bundle();
					 					b.putString("deviceid", deviceid);
					 					b.putString("ipid", ipid);
					 					b.putString("phoneno", phoneno);
					 					b.putLong("time", time);
					 					b.putLong("manualtimer", manualPollUpdatesTime);
					 					gpsService.putExtras(b);
					 					
					 					startService(gpsService);
					 					Toast.makeText(getApplicationContext(), "You Are Being Tracked", Toast.LENGTH_LONG).show(); 
					 					 ad.cancel();
				 					     finish();
											}
									}
								});
								 
								 
		 					 }catch (Exception e) {
								
							}
		 
		 }
		}catch (Exception e) {
			try {
				FileOutputStream f = new FileOutputStream(new File(root, fileName),true);
				StackTraceElement s[] = e.getStackTrace();
				Calendar c = Calendar.getInstance(); 
				String writeCard ="*********"+ c.get(Calendar.HOUR)+":"+c.get(Calendar.MINUTE)+":"+c.get(Calendar.SECOND)+"  ";
				for(int i=0;i<s.length;i++) {
					String errorData = s[i].toString();
					if(errorData.contains("com.quad.vehicle")) {
						writeCard = writeCard + errorData;
						break;
					}
				}
				writeCard = writeCard+"  "+e.getClass()+":"+e.getMessage();
				
				byte b[] = new byte[writeCard.length()];
				b = writeCard.getBytes();
				f.write(b);		
				
			} catch (Exception e1) { 
				
			}
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}



	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		
	}



	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		
	}



	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}



	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
	}



	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		
	}
	
	
	
	
	
	public void showSettingsAlert(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
   	 
        // Setting Dialog Title
        alertDialog.setTitle("GPS Settings");
 
        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Go to settings menu?");
        alertDialog.setCancelable(false);
 
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
            	Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            	startActivity(intent);
            	
            }
        });
 
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
				finish();
            }
        });
 
        // Showing Alert Message
        AlertDialog alertDialog1=alertDialog.create();
        alertDialog1.show();
	}


}
