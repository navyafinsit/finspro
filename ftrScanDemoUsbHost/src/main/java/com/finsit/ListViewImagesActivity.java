package com.finsit;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.finsit.R;

public class ListViewImagesActivity extends Activity {
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        CrimeDetailsBean cdb=new CrimeDetailsBean();
        DBHelper dbh=new DBHelper(getApplicationContext());
        ArrayList<CrimeDetailsBean> crimesList = dbh.getAllCotacts();
        final ListView lv1 = (ListView) findViewById(R.id.listV_main);
        lv1.setAdapter(new ItemListBaseAdapter(this, crimesList));
        
        lv1.setOnItemClickListener(new OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> a, View v, int position, long id) { 
        		Object o = lv1.getItemAtPosition(position);
        		CrimeDetailsBean obj_itemDetails = (CrimeDetailsBean)o;
        		Toast.makeText(ListViewImagesActivity.this, "You have chosen : " + " " + obj_itemDetails.getFileName(), Toast.LENGTH_LONG).show();
        		Intent intent=new Intent(ListViewImagesActivity.this,StartPage.class);
        		intent.putExtra("id",obj_itemDetails.getId());
        		intent.putExtra("year",obj_itemDetails.getYear());
        		intent.putExtra("month",obj_itemDetails.getMonth());
        		intent.putExtra("day",obj_itemDetails.getDay());
        		intent.putExtra("time",obj_itemDetails.getTime());
        		intent.putExtra("latitude",obj_itemDetails.getLatitude());
        		intent.putExtra("longitude",obj_itemDetails.getLongitude());
        		intent.putExtra("title",obj_itemDetails.getTitle());
        		intent.putExtra("details",obj_itemDetails.getDetails());
        		intent.putExtra("userId",obj_itemDetails.getUserid());
        		intent.putExtra("fileName",obj_itemDetails.getFileName());
        		intent.putExtra("image", obj_itemDetails.getImg());
        		intent.putExtra("status",obj_itemDetails.getStatus());
        		startActivity(intent);
        		
        		
        	}  
        });
    }
 }