package com.finsit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.finsit.R;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;

public class CustomListView extends Activity  {

    public static boolean showTinInformation=false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview);



       // ArrayList<TinDetailsBean> searchResults = GetSearchResults();

        ArrayList<TinDetailsBean> searchResults=(ArrayList<TinDetailsBean>)getIntent().getSerializableExtra("results");
        final Button screenshot = (Button) findViewById(R.id.listscreenshot);
        screenshot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeScreenshot();
               // Toast.makeText(getApplicationContext(),"Clicked",Toast.LENGTH_LONG).show();
            }
        });


        final ListView lv1 = (ListView) findViewById(R.id.ListView01);
        lv1.setAdapter(new TinCustomAdapter(this, searchResults));

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               // check to see if SHOW CRIMINAL INFO is enabled or not

                if(showTinInformation)
                {
                    Object o = lv1.getItemAtPosition(position);
                    TinDetailsBean fullObject = (TinDetailsBean)o;

                    Intent intent = new Intent(getApplicationContext(),ShowCirminalInfo.class);
                    Bundle info = new Bundle();
                    //ArrayList<TinDetailsBean> mas = new ArrayList<TinDetailsBean>();
                    info.putSerializable("info", fullObject);
                    intent.putExtras(info);

                    //startActivity(intent);
                    //finish();
                    startActivityForResult(intent, 0);
                }else
                {
                    alertboxToLogin("Information","Additional TIN Details Not available");
                }


               // Toast.makeText(CustomListView.this, "You have chosen: " + " " + fullObject.getName(), Toast.LENGTH_LONG).show();

            }
        });
    }

    public void alertboxToLogin(String title, String msg) {
        if (msg != null) {
            new AlertDialog.Builder(this)
                    .setMessage(msg)
                    .setTitle(title)
                    .setCancelable(true)
                    .setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton){
                                    //finish();
                                }
                            }).show();
        }
    }



    private ArrayList<TinDetailsBean> GetSearchResults(){
        ArrayList<TinDetailsBean> results = new ArrayList<TinDetailsBean>();


        //SearchResults sr1 = new SearchResults();
        TinDetailsBean sr1 = new TinDetailsBean();
        sr1.setName("John Smith");
        sr1.setFathersName("Dallas, TX");
        sr1.setAddress("214-555-1234");
        results.add(sr1);

        TinDetailsBean sr2 = new TinDetailsBean();
       // sr2 = new SearchResults();
        sr2.setName("Jane Doe");
        sr2.setFathersName("Atlanta, GA");
        sr2.setAddress("469-555-2587");
        results.add(sr1);

        TinDetailsBean sr3 = new TinDetailsBean();
        sr3.setName("Steve Young");
        sr3.setFathersName("Miami, FL");
        sr3.setAddress("305-555-7895");
        results.add(sr1);



        return results;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Toast.makeText(getApplicationContext(),"Called",Toast.LENGTH_LONG).show();
    }


    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        try {
            // image naming and path  to include sd card  appending name you choose for file
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + now + ".jpg";

            // create bitmap screen capture
            View v1 = getWindow().getDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            openScreenshot(imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
    }


    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(uri, "image/*");
        startActivity(intent);
    }


}