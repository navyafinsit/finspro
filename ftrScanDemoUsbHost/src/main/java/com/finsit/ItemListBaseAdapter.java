package com.finsit;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.finsit.R;

public class ItemListBaseAdapter extends BaseAdapter {
	private static ArrayList<CrimeDetailsBean> itemDetailsrrayList;
	private LayoutInflater l_Inflater;
	public ItemListBaseAdapter(Context context, ArrayList<CrimeDetailsBean> results) {
		itemDetailsrrayList = results;
		l_Inflater = LayoutInflater.from(context);
	}

	public int getCount() {
		return itemDetailsrrayList.size();
	}

	public Object getItem(int position) {
		return itemDetailsrrayList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = l_Inflater.inflate(R.layout.item_details_view, null);
			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.title);
			holder.details = (TextView) convertView.findViewById(R.id.details);
			holder.time = (TextView) convertView.findViewById(R.id.time);
			holder.photo = (ImageView) convertView.findViewById(R.id.photo);
			holder.status = (TextView) convertView.findViewById(R.id.status);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		//Creating date format
		String date=itemDetailsrrayList.get(position).getDay()+"-"+itemDetailsrrayList.get(position).getMonth()+"-"+itemDetailsrrayList.get(position).getYear()+" "+itemDetailsrrayList.get(position).getTime();
		System.out.println("Date is:"+date);
		//converting byte array into Image Bitmap
		ByteArrayInputStream imageStream = new ByteArrayInputStream(itemDetailsrrayList.get(position).getImg());
		Bitmap theImage= BitmapFactory.decodeStream(imageStream);
		String uploadStatus=itemDetailsrrayList.get(position).getStatus();
		holder.title.setText(itemDetailsrrayList.get(position).getTitle());
		holder.details.setText(itemDetailsrrayList.get(position).getDetails());
		holder.time.setText("Date:"+date);
		holder.photo.setImageBitmap(theImage);
		if(uploadStatus.equalsIgnoreCase("S")){
			holder.status.setText("Status:Success");
			holder.status.setTextColor(Color.parseColor("#3399FF"));
		}else{
			holder.status.setText("Status:Failed");
			holder.status.setTextColor(Color.parseColor("#FF6600"));
		}
		return convertView;
	}

	static class ViewHolder {
		TextView title;
		TextView details;
		TextView time;
		ImageView photo;
		TextView status;
	}
}
