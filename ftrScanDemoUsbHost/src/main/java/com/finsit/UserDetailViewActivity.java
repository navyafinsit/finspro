package com.finsit;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class UserDetailViewActivity extends Activity implements SampleDialogFragment.SampleDialogListener{


    TextView firstName;
    TextView mobileNo;
    TextView address;
    CircleImageView circleImageView;
    Button regsub;
    UserModel userModel;
    SharedPreferences data;
    Context myContext;
   // List<UserModel> userModel;
   public static byte[] mImageFP = null;
    DialogFragment serverProcessDialogFragment;

    public static int mImageWidth = 0;
    public static int mImageHeight = 0;
    String deviceId;
    Button crimanal;

    ArrayList<TinDetailsBean> tinResults = new ArrayList<TinDetailsBean>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_reg);

    //SqliteHelper sqliteHelper=new SqliteHelper(this);
      //  final List<UserModel> userList=sqliteHelper.getAllUsers();

     userModel= (UserModel) getIntent().getExtras().getParcelable("userObject");
     data = getSharedPreferences("datastore", Context.MODE_PRIVATE);
        deviceId = data.getString("deviceid", null);

     firstName=(TextView)findViewById(R.id.firstName);
     mobileNo=(TextView)findViewById(R.id.mobilenumber);
     address=(TextView)findViewById(R.id.address);
     circleImageView=(CircleImageView) findViewById(R.id.circleImageViewProfile);
        regsub=(Button)findViewById(R.id.regsub);
        crimanal=(Button)findViewById(R.id.crimnal);

     firstName.setText(userModel.getName());
     mobileNo.setText(userModel.getMobileno());
     address.setText(userModel.getAddress());

      byte[] byteArray=userModel.getProfile();
      Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
      circleImageView.setImageBitmap(bitmap);
        serverProcessDialogFragment = new SampleDialogFragment();
     final byte[] fingerByteArray=userModel.getFingerImage();
     final Bitmap fingerBitmap = BitmapFactory.decodeByteArray(fingerByteArray, 0, fingerByteArray.length);



                regsub.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View arg0) {


                        ConnectionDetector connectionDetector=new ConnectionDetector(UserDetailViewActivity.this);
                        if(!connectionDetector.isConnectingToInternet()) {
                            alertboxToLogin("Internet Connectivity Failed", "Check Internet Connection.");
                            //finish();
                        }else {
                        OnBtnVerifyIsoScanner(fingerBitmap);
                        }
                    }


                });



    }

    public void alertboxToLogin(String title, String msg) {
        if (msg != null) {
            new AlertDialog.Builder(this)
                    .setMessage(msg)
                    .setTitle(title)
                    .setCancelable(true)
                    .setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton){

                                }
                            }).show();
        }
    }


    public void OnBtnVerifyIsoScanner(Bitmap bitmap) {
        //Toast.makeText(getApplicationContext(), img_fp_src.getDrawable(), 5).show();
        try {

            //futronics code
           // MyBitmapFile fileBMP = new MyBitmapFile(mImageWidth, mImageHeight, mImageFP);
            //Bitmap bitmap = BitmapFactory.decodeByteArray(fileBMP.toBytes(), 0, fileBMP.toBytes().length);
         /*   Drawable d = FtrScanDemoUsbHostActivity.mFingerImage.getDrawable();
            Bitmap bitmap = ((BitmapDrawable)d).getBitmap();*/
            //MyBitmapFile fileBMP = new MyBitmapFile(320, 480,byteArray );
            //Bitmap bitmap = BitmapFactory.decodeByteArray(fileBMP.toBytes(), 0, fileBMP.toBytes().length);
            bitmap= Bitmap.createScaledBitmap(bitmap, 320, 480, false);

            if (bitmap != null) {
                System.out.println("Entered into image save option");
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 25, stream);
                bitmap.setDensity(500);

                byte[] imageInByte = stream.toByteArray();
                //Toast.makeText(CrimeReportActivity.this, "Image attached", Toast.LENGTH_LONG).show();
                //ByteArrayInputStream bis = new ByteArrayInputStream(imageInByte);

                // added by dinesh for IMAGE INTERNAL MEMORY
                ContextWrapper cw = new ContextWrapper(getApplicationContext());
                // path to /data/data/yourapp/app_data/imageDir
                File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
                // Create imageDir
                File mypath = new File(directory, "template.dat");


                FileOutputStream fos = new FileOutputStream(mypath.getAbsolutePath());

                //FileOutputStream fos = new FileOutputStream("/storage/sdcard0/template.dat");
                fos.write(imageInByte);
                fos.close();
                serverProcessDialogFragment.show(getFragmentManager(), "DIALOG_TYPE_PROGRESS");
                serverProcessDialogFragment.setCancelable(false);

               UploadTask task = new UploadTask();
                //task.execute("/storage/sdcard0/template.dat");
                task.execute(mypath.getAbsolutePath());
                //tvInfo.setText(output);
                //Toast.makeText(getApplicationContext(), output, Toast.LENGTH_LONG).show();
             } else {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClickStopBtn(DialogFragment dialogFragment) {

    }

    private class UploadTask extends AsyncTask<String, Void, String> {
        String output = null;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... urls) {
            for (String url : urls) {
                output = upload(url);

            }
	       /* if(output!=null){
	        	onPostExecute(output);
	        }*/
            return output;
        }


        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            //String criminalName = null;
            //final ImageView imageView = (ImageView) findViewById(R.id.criminalImage);
            //final TextView nameOfTheCriminal=(TextView) findViewById(R.id.criminalName);
            System.out.println("Entered into on post executes" + result);
            crimanal.setVisibility(android.view.View.VISIBLE);

            //added to check if the device is authorized to communicate to the server

            if (result != null && "".equals(result.trim())) {
                //tvInfo.setText("NO MATCHES FOUND");
                crimanal.setEnabled(true);
                crimanal.setText("No Match. Enroll?");
                crimanal.setBackgroundColor(Color.RED);

                serverProcessDialogFragment.dismiss();

                return;
            } else {
                serverProcessDialogFragment.dismiss();

                if ("DISABLED".equalsIgnoreCase(result.trim())) {
                    //Context context = getApplicationContext();
				/*	System.out.println("** DISABLED **");
					CharSequence text = "YOUR DEVICE IS DISABLED";
					int duration = Toast.LENGTH_LONG;

					Toast toast = Toast.makeText(context, text, duration);
					toast.show();*/

                    alertboxToConfigure("CONFIGURATION VOILATION", "YOUR DEVICE IS DISABLED");

                    return;
                }

                if ("UNAUTHORIZED".equalsIgnoreCase(result.trim())) {
					/*//Context context = getApplicationContext();
					CharSequence text = "YOUR DEVICE IS NOT AUTHORIZED";
					int duration = Toast.LENGTH_LONG;

					Toast toast = Toast.makeText(context, text, duration);
					toast.show();*/
                    alertboxToConfigure("CONFIGURATION VOILATION", "YOUR DEVICE IS NOT AUTHORIZED");
                    return;
                }

                if ("BADOBJECT".equalsIgnoreCase(result.trim())) {
					/*//Context context = getApplicationContext();
					CharSequence text = "YOUR DEVICE IS NOT AUTHORIZED";
					int duration = Toast.LENGTH_LONG;

					Toast toast = Toast.makeText(context, text, duration);
					toast.show();*/
                    alertboxToConfigure("BAD FINGERPRINT QUALITY", "Please Scan Fingerprint Properly");
                    return;
                }

            }
            //tvInfo.setText(output);
            try {
                tinResults.clear();
                if (result != null) {

                    JSONArray jArray = new JSONArray(result);

                    for (int i = 0; i < jArray.length(); i++) {
                        JSONObject obj = jArray.getJSONObject(i);

                        TinDetailsBean tbean = new TinDetailsBean();


                        System.out.println("JSON OBJECT IS" + obj);

                        tbean.setName(obj.getString("CriminalName"));
                        tbean.setFathersName(obj.getString("phoneno"));
                        tbean.setAddress(obj.getString("address"));
                        tbean.setState(obj.getString("state"));
                        tbean.setDistrict(obj.getString("district"));
                        tbean.setType(obj.getString("type"));
                        tbean.setTinNumber(obj.getString("tinNumber"));
                        tbean.setPoliceStation(obj.getString("policestation"));
                        tbean.setWebUrl(obj.getString("WebUploadedImageURL"));
                        tbean.setTemplateURL(obj.getString("templateURL"));


                        tinResults.add(tbean);
						/*	criminalName=obj.getString("CriminalName");
							templateURL=obj.getString("templateURL");
							address=obj.getString("address");
							phoneNo=obj.getString("phoneno");
							latitude=obj.getString("latitude");
							longtitude=obj.getString("longtitude");
							personCategory=obj.getString("criminalCategory");
							webImage=obj.getString("WebUploadedImageURL");*/

                    }


                    //System.out.println("json recieved string:"+jsonString);
                    //Blob blob = (Blob) obj.get("image");
                    //int blobLength = (int) blob.length();
                    //final byte[] imageBytes = blob.getBytes(1, blobLength);
                    //final byte[] imageBytes =(byte[]) obj.get("image");
                    //criminalName=obj.getString("CriminalName");
                    if (jArray.length() > 0) {
                        //tvInfo.setText("Matched Name is:"+criminalName);
                        crimanal.setEnabled(true);
                        crimanal.setText("Get Criminal Info");
                        crimanal.setBackgroundColor(Color.GREEN);
                    }
                }


				/*tvInfo.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						try {
						try {// TODO Auto-generated method stub
						setContentView(R.layout.more_information);
						ByteArrayInputStream imageStream = new ByteArrayInputStream(imageBytes);
						Bitmap theImage= BitmapFactory.decodeStream(imageStream);
						URL url = new URL(address);
						InputStream content = (InputStream)url.getContent();
						Drawable d = Drawable.createFromStream(content , "src");
						iv.setImageDrawable(d)
						imageView.setImageBitmap(theImage);

						//nameOfTheCriminal.setText(obj.getString("CriminalName"));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});*/
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (Exception e2) {
                System.out.println("exception occured");
                e2.printStackTrace();
            }
            serverProcessDialogFragment.dismiss();

        }

        public String convertStreamToString(InputStream is) throws Exception {

            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            return sb.toString();
        }

        public String upload(String i_file) {
            String responseString = null;
            try {
                System.out.println("ENTERED INTO UPLOAD PICTURE TO SERVER for :" + deviceId);
                HttpClient httpclient = new DefaultHttpClient();
                httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
                String imagePath = "Template" + System.currentTimeMillis();
                if (imagePath != null && deviceId != null) {
                    System.out.println("Hitting Server");

                    //sending the imei number
                    TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    if (ActivityCompat.checkSelfPermission(UserDetailViewActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.

                    }
                    String IMEINumber = mngr.getDeviceId();

                    //HttpPost httppost = new HttpPost("http://137.59.201.89:9091/finsprows/GetPictureFromClient?imageUrl="+imagePath+"&deviceId="+deviceId);

                    double lattitude = 0;
                    double longitude = 0;
                    Location lastKnownLocation = null;


                    LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                    List<String> providers = locationManager.getProviders(true);

                    Location l = null;
                    for (int i = 0; i < providers.size(); i++) {
                        if (ActivityCompat.checkSelfPermission(UserDetailViewActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(UserDetailViewActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.

                        }
                        l = locationManager.getLastKnownLocation(providers.get(i));
                        if (l != null)
                            break;
                    }
                    if (l != null) {
                        lattitude = l.getLatitude();
                        longitude = l.getLongitude();
                    }


					/*if(locationManager != null) {
						lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
					}*/



                    //HttpPost httppost = new HttpPost("http://137.59.201.89:9091/finsprows/GetPictureFromClient?imageUrl="+imagePath+"&deviceId="+deviceId+"&imei="+IMEINumber);

                    HttpPost httppost = new HttpPost(IPInfo.HTTP_IPADDRESS+"/finsprows/GetPictureFromClient?imageUrl="+imagePath+"&deviceId="+deviceId+"&imei="+IMEINumber+"&lat="+lattitude+"&lon="+longitude);

                    //	HttpPost httppost = new HttpPost("http://198.27.68.95:9091/finsprows/GetPictureFromClient?imageUrl="+imagePath+"&deviceId="+deviceId+"&imei="+IMEINumber+"&lat="+lattitude+"&lon="+longitude);


                    File file = new File(i_file);
                    MultipartEntity mpEntity = new MultipartEntity();
                    ContentBody cbFile = new FileBody(file, "image/jpeg");
                    mpEntity.addPart("userfile", cbFile);
                    httppost.setEntity(mpEntity);
                    System.out.println("executing request " + httppost.getRequestLine());
                    //tvInfo.setText("executing request !!");
                    HttpResponse response = httpclient.execute(httppost);
                    //tvInfo.setText("Response Got...processing now!!");
                    HttpEntity resEntity = response.getEntity();
                    InputStream stream=resEntity.getContent();
                    responseString = convertStreamToString(stream);
                    System.out.println("RESPONSE STRING IS:"+responseString);
                    //tvInfo.setText("Response Finished!!");
                    //Toast.makeText(getApplicationContext(),"Hello"+EntityUtils.toString(resEntity),Toast.LENGTH_LONG).show();
                    System.out.println(response.getStatusLine());
	/*			    if (responseString != null) {
				      //System.out.println(EntityUtils.toString(resEntity));
				      //tvInfo.setText(EntityUtils.toString(resEntity));
				      //Toast.makeText(getApplicationContext(),EntityUtils.toString(resEntity),Toast.LENGTH_LONG).show();
				    }
				    if(responseString==null){
				    	responseString="NO MATCHES FOUND";
				    }*/
                    if (resEntity != null) {
                        resEntity.consumeContent();
                    }
                    httpclient.getConnectionManager().shutdown();
                }
            }catch(Exception e)
            {
                e.printStackTrace();
            }
            return responseString;

        }
    }
    public void alertboxToConfigure(String title, final String msg) {
        if (msg != null) {
            new AlertDialog.Builder(this)
                    .setMessage(msg)
                    .setTitle(title)
                    .setCancelable(true)
                    .setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
									/*if(msg.contains("Device Configured Successfully"))
									{
										Intent i=new Intent(TestgpsActivity.this,FtrScanDemoUsbHostActivity.class);
										startActivity(i);
										finish();
									}*/
		    		/*  else
		    		  {
		    			  Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
		    			  return;
		    		  }*/
                                }
                            }).show();
        }
    }

}
