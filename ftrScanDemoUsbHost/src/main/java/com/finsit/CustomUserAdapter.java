package com.finsit;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class CustomUserAdapter extends BaseAdapter  {
    private static List<UserModel> usersList;
    private LayoutInflater mInflater;
    Context context;
    SqliteHelper sqliteHelper;

    public CustomUserAdapter(Context context, List<UserModel> results) {
        usersList = results;
        mInflater = LayoutInflater.from(context);
        this.context=context;
        sqliteHelper = new SqliteHelper(context);

    }



    public int getCount() {
        return usersList.size();
    }

    public Object getItem(int position) {
        return usersList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_list_row, null);
            holder = new ViewHolder();
            holder.img=(CircleImageView) convertView.findViewById(R.id.img);
            holder.firstName = (TextView) convertView.findViewById(R.id.txt);
            holder.mobile = (TextView) convertView.findViewById(R.id.mobile);
            holder.addrss = (TextView) convertView.findViewById(R.id.adrss);
            holder.delete = (Button) convertView.findViewById(R.id.del);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        byte[] byteArray=usersList.get(position).getProfile();
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        holder.img.setImageBitmap(bitmap);

        holder.firstName.setText(usersList.get(position).getName());
        holder.mobile.setText(usersList.get(position).getMobileno());
        holder.addrss.setText(usersList.get(position).getAddress());
       // holder.delete.setTag(position);
        holder.deleteId=""+usersList.get(position).getId();


        holder.delete.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                AlertDialog.Builder myDialog = new AlertDialog.Builder(context);

                myDialog.setTitle("Do You want Delete");
                  myDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        sqliteHelper.deleteUserRecord(holder.deleteId);
                        usersList= sqliteHelper.getAllUsers();
                        notifyDataSetChanged();
                    }
                });

                myDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

                myDialog.show();


              //  sqliteHelper.deleteUserRecord(holder.deleteId);
             //   usersList= sqliteHelper.getAllUsers();
             //   notifyDataSetChanged();



            }


        });




        return convertView;
    }



    static class ViewHolder {
        CircleImageView img;
        TextView firstName;
        TextView mobile;
        TextView addrss;
        Button delete;
        String deleteId;




    }


}