package com.finsit;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    //private final String[] web;
   // private final Integer[] imageId;
    List<UserModel> usersList;

    public CustomListAdapter(Activity context,
                             List<UserModel> usersList) {
        super(context, R.layout.list_single);
        this.context = context;
        this.usersList=usersList;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.list_single, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        txtTitle.setText(usersList.get(position).getName());

        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        byte[] byteArray=usersList.get(position).getProfile();
        Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        imageView.setImageBitmap(bitmap);
     return rowView;
    }
}