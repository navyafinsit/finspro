package com.finsit;

import android.app.Activity;
public class CrimeReportActivity extends Activity{
	
}

/*@SuppressWarnings("unused")
public class CrimeReportActivity extends Activity implements NBioBSPJNI.CAPTURE_CALLBACK, SampleDialogListener, UserDialogListener,View.OnClickListener,DialogInterface.OnClickListener{
	private static final String TAG = CrimeReportActivity.class.getSimpleName();
	private NBioBSPJNI				bsp;
	private NBioBSPJNI.Export       exportEngine;
	private NBioBSPJNI.IndexSearch  indexSearch;
	private byte[]					byTemplate1;
	private byte[]					byCapturedRaw1;
	private int						nCapturedRawWidth1;
	private int						nCapturedRawHeight1;
	private int						nCapturedRawWidth2;
	private int						nCapturedRawHeight2;
	ImageView img_fp_src; 
	TextView tvInfo, tvVer, tvDevice,deviceIdFromPCATS;	
	Button btnCapture1,btnVerifyTemplate;
	Button moreButton;
	DialogFragment sampleDialogFragment;
	DialogFragment serverProcessDialogFragment;
	UserDialog userDialog;
	String criminalName;
	String templateURL;
	String address;
	String phoneNo;
	String deviceId;
	String latitude;
	String longtitude;
	String personCategory;
	String webImage;
	private boolean					bCapturedFirst, bAutoOn = false;
	public static final int QUALITY_LIMIT = 80;
	Context myContext;
	SharedPreferences data;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
			myContext = createPackageContext("com.quad.track",Context.CONTEXT_IGNORE_SECURITY);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			Toast.makeText(getApplicationContext(), "Please Install PCATS Application",Toast.LENGTH_LONG).show();
			finish();
			return;
		}
        initView();
        initData();
    }
    *//**
     * void
     *//*
    public void initView(){
    	setContentView(R.layout.fingerprint_upload);
    	img_fp_src = (ImageView)findViewById(R.id.img_fp_src);
    	img_fp_src.setBackgroundColor(Color.argb(255, 255, 255, 255));
    	tvInfo = (TextView) findViewById(R.id.textInfo);
    	//tvVer = (TextView) findViewById(R.id.textVer);
    	tvDevice = (TextView) findViewById(R.id.textDevice);
        btnCapture1 = (Button) findViewById(R.id.btnCapture1);
    	btnCapture1.setEnabled(false);
    	btnCapture1.setBackgroundResource(R.drawable.custom_button_grey);
    	btnVerifyTemplate = (Button) findViewById(R.id.btnVerifyIso);
    	btnVerifyTemplate.setEnabled(false);
    	btnVerifyTemplate.setBackgroundResource(R.drawable.custom_button_grey);
    	data = myContext.getSharedPreferences("datastore", Context.MODE_PRIVATE);
		deviceId=data.getString("deviceid",null);
		if(deviceId!=null){
			System.out.println("DEVICE ID IS:"+deviceId);
			deviceIdFromPCATS=(TextView) findViewById(R.id.deviceIdFromPCATS);
			deviceIdFromPCATS.setText("Device:"+deviceId.toString());
		}
		else{
			Toast.makeText(getApplicationContext(), "Please Install PCATS Application",Toast.LENGTH_LONG).show();
			finish();
			return;
		}    	
    	moreButton=(Button)findViewById(R.id.moreButton);
    	moreButton.setEnabled(false);
    	moreButton.setBackgroundResource(R.drawable.custom_button_grey);
    	moreButton.setOnClickListener(this);
    }
    public void initData(){
    	NBioBSPJNI.CURRENT_PRODUCT_ID = 0;
    	if(bsp==null){    	
    		bsp = new NBioBSPJNI("010701-613E5C7F4CC7C4B0-72E340B47E034015", this);
    		String msg = null;
    		if (bsp.IsErrorOccured())
    			msg = "Scanner Error: " + bsp.GetErrorCode();
    		else  {
    			msg = "SDK Version: " + bsp.GetVersion();
    			exportEngine = bsp.new Export();
    			indexSearch = bsp.new IndexSearch();
    		}
    		//tvVer.setText(msg);
    	}
    	sampleDialogFragment = new SampleDialogFragment();
    	userDialog = new UserDialog();
    	serverProcessDialogFragment=new SampleDialogFragment();
    }
    @Override
    public void onDestroy(){
    	super.onDestroy();
        if (bsp != null) {
            bsp.dispose();
            bsp = null;
        }
    }
    public int OnCaptured(CAPTURED_DATA capturedData){
    	tvDevice.setText("IMAGE Quality: "+capturedData.getImageQuality());	
    	if( capturedData.getImage()!=null){    		
    			img_fp_src.setImageBitmap( capturedData.getImage());
    			//moreButton.setEnabled(false);
    			//capturedData.getImage().setDensity(500);
    	}
    	// quality : 40~100
    	if(capturedData.getImageQuality()>=QUALITY_LIMIT){
    		if(sampleDialogFragment!=null && "DIALOG_TYPE_PROGRESS".equals(sampleDialogFragment.getTag()))
    			sampleDialogFragment.dismiss();
    		return NBioBSPJNI.ERROR.NBioAPIERROR_USER_CANCEL;
    	}else if(capturedData.getDeviceError()!=NBioBSPJNI.ERROR.NBioAPIERROR_NONE){
    		if(sampleDialogFragment!=null && "DIALOG_TYPE_PROGRESS".equals(sampleDialogFragment.getTag()))
    			sampleDialogFragment.dismiss();
    		return capturedData.getDeviceError();
    	}else{
    		return NBioBSPJNI.ERROR.NBioAPIERROR_NONE;    		
    	}
    }
	 (non-Javadoc)
	 * @see com.nitgen.SDK.AndroidBSP.SampleDialogFragment.SampleDialogListener#onClickStopBtn(android.app.DialogFragment)
	 
	public void onClickStopBtn(DialogFragment dialogFragment) {
		bAutoOn = false;
		sampleDialogFragment.dismiss();
	}

	public void OnConnected() {
		
		if(sampleDialogFragment!=null)
			sampleDialogFragment.dismiss();
		
		String message = "Device Open Success";
		tvDevice.setText(message);
		btnCapture1.setEnabled(true);
		btnVerifyTemplate.setEnabled(false);
		btnCapture1.setBackgroundResource(R.drawable.custom_button);
		//btnVerifyTemplate.setBackgroundResource(R.drawable.custom_button);
      
		
	}
	
	public void OnDisConnected(){
		
		NBioBSPJNI.CURRENT_PRODUCT_ID = 0;
		
		if(sampleDialogFragment!=null)
			sampleDialogFragment.dismiss();
		
		String message = "Scanner Disconnected: " + bsp.GetErrorCode();
		tvDevice.setText(message);
		
		btnCapture1.setEnabled(false);
		btnVerifyTemplate.setEnabled(false);
      
		btnCapture1.setBackgroundResource(R.drawable.custom_button_grey);
		btnVerifyTemplate.setBackgroundResource(R.drawable.custom_button_grey);
		
	}

    *//**
     * void
     *//*
    public void OnBtnOpenDevice(View target){

    	sampleDialogFragment.show(getFragmentManager(), "DIALOG_TYPE_PROGRESS");		    	
    	bsp.OpenDevice();

    }
    
   
    
    
	*//**
	 * void
	 *//*
	public void OnBtnCapture1(View target) {
		
		sampleDialogFragment.show(getFragmentManager(), "DIALOG_TYPE_PROGRESS");
		sampleDialogFragment.setCancelable(false);
		tvInfo.setBackgroundColor(Color.WHITE);
		new Thread(new Runnable() {
			
			public void run() {
				OnCapture1(10000);
			}
		}).start();
				
	}
	
	String msg = "";
	public synchronized void OnCapture1(int timeout){
		NBioBSPJNI.FIR_HANDLE hCapturedFIR, hAuditFIR;
    	NBioBSPJNI.CAPTURED_DATA capturedData;
    	hCapturedFIR = bsp.new FIR_HANDLE();
    	hAuditFIR = bsp.new FIR_HANDLE();
    	capturedData = bsp.new CAPTURED_DATA();
    	bCapturedFirst = true;
		bsp.Capture(NBioBSPJNI.FIR_PURPOSE.ENROLL,hCapturedFIR,timeout, hAuditFIR, capturedData, CrimeReportActivity.this,0, null);
		if(sampleDialogFragment!=null && "DIALOG_TYPE_PROGRESS".equals(sampleDialogFragment.getTag()))
			sampleDialogFragment.dismiss();
		if (bsp.IsErrorOccured())  {
        	msg = "Scanner Capture Error: " + bsp.GetErrorCode();
        }
        else  {
        	NBioBSPJNI.INPUT_FIR inputFIR;
        	inputFIR = bsp.new INPUT_FIR();
        	// Make ISO 19794-2 data
        	{
        		NBioBSPJNI.Export.DATA exportData;
        		inputFIR.SetFIRHandle(hCapturedFIR);
        		exportData = exportEngine.new DATA();
        		exportEngine.ExportFIR(inputFIR, exportData, NBioBSPJNI.EXPORT_MINCONV_TYPE.OLD_FDA);
        		if (bsp.IsErrorOccured())  {
        			runOnUiThread(new Runnable() {
						public void run() {
							msg = "Scanner ExportFIR Error: " + bsp.GetErrorCode();
							tvInfo.setText(msg);
							Toast.makeText(CrimeReportActivity.this, msg, Toast.LENGTH_SHORT).show();
						}
					});
            		return ;
            	}
        		if (byTemplate1 != null)
        			byTemplate1 = null;
        		byTemplate1 = new byte[exportData.FingerData[0].Template[0].Data.length];
        		byTemplate1 = exportData.FingerData[0].Template[0].Data;
        	}
        	// Make Raw Image data
        	{
        		NBioBSPJNI.Export.AUDIT exportAudit;
        		inputFIR.SetFIRHandle(hAuditFIR);
        		exportAudit = exportEngine.new AUDIT();
        		exportEngine.ExportAudit(inputFIR, exportAudit);
        		if (bsp.IsErrorOccured())  {
        			runOnUiThread(new Runnable() {
						public void run() {
							msg = "Scanner ExportAudit Error: " + bsp.GetErrorCode();
							tvInfo.setText(msg);
							Toast.makeText(CrimeReportActivity.this, msg, Toast.LENGTH_SHORT).show();
						}
					});
            		return ;
            	}
        		if (byCapturedRaw1 != null)
        			byCapturedRaw1 = null;
        		byCapturedRaw1 = new byte[exportAudit.FingerData[0].Template[0].Data.length];
        		byCapturedRaw1 = exportAudit.FingerData[0].Template[0].Data;
    			nCapturedRawWidth1 = exportAudit.ImageWidth;
    			nCapturedRawHeight1 = exportAudit.ImageHeight;
				msg = "First Capture Success";
        	}
        }
		runOnUiThread(new Runnable() {
			public void run() {
				tvInfo.setText(msg);
				if (byTemplate1 != null )  {
					btnVerifyTemplate.setEnabled(true);
					btnVerifyTemplate.setBackgroundResource(R.drawable.custom_button);
				}else{
					btnVerifyTemplate.setEnabled(false);
					btnVerifyTemplate.setBackgroundResource(R.drawable.custom_button_grey);
				}
			}
		});
	}
    *//**
     * void
     *//*
    public void OnBtnVerifyIso(View target){
    	//Toast.makeText(getApplicationContext(), img_fp_src.getDrawable(), 5).show();
    	try{
    	BitmapDrawable bitmapDrawable = ((BitmapDrawable) img_fp_src.getDrawable());
    	Bitmap bitmap = bitmapDrawable.getBitmap();
    	if(bitmap!=null){
    		System.out.println("Entered into image save option");
        	ByteArrayOutputStream stream = new ByteArrayOutputStream();
        	bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        	bitmap.setDensity(500);
        	byte[] imageInByte = stream.toByteArray();
        	//Toast.makeText(CrimeReportActivity.this, "Image attached", Toast.LENGTH_LONG).show();
        	//ByteArrayInputStream bis = new ByteArrayInputStream(imageInByte);
        	FileOutputStream fos = new FileOutputStream("/storage/sdcard0/template.dat");
        	fos.write(imageInByte);
        	fos.close();
        	serverProcessDialogFragment.show(getFragmentManager(), "DIALOG_TYPE_PROGRESS");		
        	UploadTask task=new UploadTask();
        	task.execute("/storage/sdcard0/template.dat");
        	//tvInfo.setText(output);
        	//Toast.makeText(getApplicationContext(), output, Toast.LENGTH_LONG).show();
    	}else{
    		return;
    	}
    	}catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

    	MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_android__demo, menu);
    	
    	return true;
    }
    
     (non-Javadoc)
     * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
     
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

    	switch(item.getItemId()){
    	case R.id.menu_add_fir:
    		
    		userDialog.show(getFragmentManager(), "add_fir");		    	
    		
    		
    		return true;
    	case R.id.menu_identify:
    		
    		OnIdentify(5000);
    		
    		return true;
    	case R.id.menu_remove:
    		userDialog.show(getFragmentManager(), "remove");		    	
    		
    		return true;
    	}
    	
    	return false;
    }
    
	public synchronized void OnAddFIR(int timeout, String id){

		NBioBSPJNI.FIR_HANDLE hCapturedFIR, hAuditFIR;
    	NBioBSPJNI.CAPTURED_DATA capturedData;
    	hCapturedFIR = bsp.new FIR_HANDLE();
    	hAuditFIR = bsp.new FIR_HANDLE();
    	capturedData = bsp.new CAPTURED_DATA();
		
    	bsp.Capture(NBioBSPJNI.FIR_PURPOSE.ENROLL,hCapturedFIR,timeout, hAuditFIR, capturedData, this,0,null);

		if (bsp.IsErrorOccured())  {
        	msg = "Scanner Capture Error: " + bsp.GetErrorCode();
        }else  {

        	NBioBSPJNI.INPUT_FIR inputFIR;
        	
        	inputFIR = bsp.new INPUT_FIR();
        	
        	inputFIR.SetFIRHandle(hCapturedFIR);

    		SAMPLE_INFO sampleInfo = indexSearch.new SAMPLE_INFO();
    		
    		indexSearch.AddFIR(inputFIR, Integer.parseInt(id), sampleInfo);
    		if (bsp.IsErrorOccured())  {
    			
    			Toast.makeText(this, id+ " Add Failure", Toast.LENGTH_SHORT).show();
    		}else{
    			
    			Toast.makeText(this, id+ " Add Success", Toast.LENGTH_SHORT).show();
    		}

        }

	}
	
	public synchronized void OnIdentify(int timeout){
		
		NBioBSPJNI.FIR_HANDLE hCapturedFIR, hAuditFIR;
    	NBioBSPJNI.CAPTURED_DATA capturedData;
    	hCapturedFIR = bsp.new FIR_HANDLE();
    	hAuditFIR = bsp.new FIR_HANDLE();
    	capturedData = bsp.new CAPTURED_DATA();
		
    	bsp.Capture(NBioBSPJNI.FIR_PURPOSE.ENROLL,hCapturedFIR,timeout, hAuditFIR, capturedData, this,0,null);

		if (bsp.IsErrorOccured())  {
        	msg = "Scanner Capture Error: " + bsp.GetErrorCode();
        }else  {

        	NBioBSPJNI.INPUT_FIR inputFIR;
        	
        	inputFIR = bsp.new INPUT_FIR();
        	
        	inputFIR.SetFIRHandle(hCapturedFIR);

        	FP_INFO fpInfo = indexSearch.new FP_INFO();
    		
    		indexSearch.Identify(inputFIR, 1, fpInfo, 2000);
    		
    		if(fpInfo.ID!=0){    			
    			Toast.makeText(this, fpInfo.ID+" Identify Success", Toast.LENGTH_SHORT).show();
    		}else{
    			Toast.makeText(this, "Identify Failure", Toast.LENGTH_SHORT).show();
    		}

        }	
		
	}
	
	public synchronized void OnRemoveUser(String id){
		
		indexSearch.RemoveUser(Integer.parseInt(id));
		if (bsp.IsErrorOccured())  {			
			Toast.makeText(this, id+" Delete Failure", Toast.LENGTH_SHORT).show();
		}else{
			
			Toast.makeText(this, id+" Delete Success", Toast.LENGTH_SHORT).show();
		}
		
	}

	 (non-Javadoc)
	 * @see com.nitgen.SDK.AndroidBSP.UserDialog.UserDialogListener#onClickPositiveBtn(android.app.DialogFragment, java.lang.String)
	 
	public void onClickPositiveBtn(DialogFragment dialogFragment, String id) {

		if("add_fir".equals(dialogFragment.getTag())){
			OnAddFIR(5000, id);
		}else if("remove".equals(dialogFragment.getTag())){
			OnRemoveUser(id);
		}
		
	}
	
	
	private class  UploadTask extends AsyncTask<String, Void, String> {
		String output=null;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

		}
		
	    @Override
	    protected String doInBackground(String... urls) {
	        for (String url : urls) {
	            output = upload(url);
	           
	        }
	        if(output!=null){
	        	onPostExecute(output);
	        }
	        return output;
	    }
	    
	   
	    
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//String criminalName = null;
			//final ImageView imageView = (ImageView) findViewById(R.id.criminalImage);
			//final TextView nameOfTheCriminal=(TextView) findViewById(R.id.criminalName);
			System.out.println("Entered into on post executes"+result);
			moreButton.setVisibility(android.view.View.VISIBLE);
		    if(result!=null && "".equals(result.trim()))
		    {
		    	tvInfo.setText("NO MATCHES FOUND");
		    	moreButton.setEnabled(true);
		    	moreButton.setText("No Match. Enroll?");
				moreButton.setBackgroundColor(Color.RED);
				serverProcessDialogFragment.dismiss();
				
				return;
		    }
			//tvInfo.setText(output);
			try {
				if(result!=null)
				{
					JSONArray jArray=new JSONArray(result);
					JSONObject obj = null;
					if(jArray.length()>0){
						obj=jArray.getJSONObject(0);
						System.out.println("JSON OBJECT IS"+obj);
						criminalName=obj.getString("CriminalName");
						templateURL=obj.getString("templateURL");
						address=obj.getString("address");
						phoneNo=obj.getString("phoneno");
						latitude=obj.getString("latitude");
						longtitude=obj.getString("longtitude");
						personCategory=obj.getString("criminalCategory");
						webImage=obj.getString("WebUploadedImageURL");
					}
					//System.out.println("json recieved string:"+jsonString);
					//Blob blob = (Blob) obj.get("image");
					//int blobLength = (int) blob.length(); 
					//final byte[] imageBytes = blob.getBytes(1, blobLength);
					//final byte[] imageBytes =(byte[]) obj.get("image");
					//criminalName=obj.getString("CriminalName");
					if(criminalName!=null){
						tvInfo.setText("Matched Name is:"+criminalName);
						moreButton.setEnabled(true);
						moreButton.setText("Get Criminal Info");
						moreButton.setBackgroundColor(Color.GREEN);
					}
				}		
				
			
				tvInfo.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						try {
						try {// TODO Auto-generated method stub
						setContentView(R.layout.more_information);
						ByteArrayInputStream imageStream = new ByteArrayInputStream(imageBytes);
						Bitmap theImage= BitmapFactory.decodeStream(imageStream);
						URL url = new URL(address);
						InputStream content = (InputStream)url.getContent();
						Drawable d = Drawable.createFromStream(content , "src"); 
						iv.setImageDrawable(d)
						imageView.setImageBitmap(theImage);

						//nameOfTheCriminal.setText(obj.getString("CriminalName"));
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (Exception e2) {
				System.out.println("exception occured");
				e2.printStackTrace();
			}
			serverProcessDialogFragment.dismiss();
		
		}
		public String convertStreamToString(InputStream is) throws Exception {

		    BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		    StringBuilder sb = new StringBuilder();
		    String line = null;
		    while ((line = reader.readLine()) != null) {
		        sb.append(line + "\n");
		    }
		    is.close();
		    return sb.toString();
		}
		public String upload(String i_file) 
		{
			String responseString = null;
			try
			{
				System.out.println("ENTERED INTO UPLOAD PICTURE TO SERVER");
			    HttpClient httpclient = new DefaultHttpClient();
			    httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			    String imagePath="Template"+System.currentTimeMillis();
			    if(imagePath!=null && deviceId!=null){
				    HttpPost httppost = new HttpPost("http://43.254.42.12:9091/finsprows/GetPictureFromClient?imageUrl="+imagePath+"&deviceId="+deviceId);
				    File file = new File(i_file);
				    MultipartEntity mpEntity = new MultipartEntity();
				    ContentBody cbFile = new FileBody(file, "image/jpeg");
				    mpEntity.addPart("userfile", cbFile);
				    httppost.setEntity(mpEntity);
				    System.out.println("executing request " + httppost.getRequestLine());
				    //tvInfo.setText("executing request !!");
				    HttpResponse response = httpclient.execute(httppost);
				    //tvInfo.setText("Response Got...processing now!!");
				    HttpEntity resEntity = response.getEntity();
				    InputStream stream=resEntity.getContent();
				    responseString = convertStreamToString(stream);
				    System.out.println("RESPONSE STRING IS:"+responseString);
				    //tvInfo.setText("Response Finished!!");
				    //Toast.makeText(getApplicationContext(),"Hello"+EntityUtils.toString(resEntity),Toast.LENGTH_LONG).show();
				    System.out.println(response.getStatusLine());
				    if (responseString != null) {
				      //System.out.println(EntityUtils.toString(resEntity));
				      //tvInfo.setText(EntityUtils.toString(resEntity));
				      //Toast.makeText(getApplicationContext(),EntityUtils.toString(resEntity),Toast.LENGTH_LONG).show();
				    }
				    if(responseString==null){
				    	responseString="NO MATCHES FOUND";
				    }
				    if (resEntity != null) {
				      resEntity.consumeContent();
				    }
				    httpclient.getConnectionManager().shutdown();
			    }
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			return responseString;
		    
		}
	}


	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub
				switch(which){
				case DialogInterface.BUTTON_POSITIVE: // yes
					System.out.println("Entered into no match state");
					Intent noMatchIntent=new Intent(this,StartPage.class);
					BitmapDrawable bitmapDrawable = ((BitmapDrawable) img_fp_src.getDrawable());
					Bitmap bitmap = bitmapDrawable.getBitmap();
					noMatchIntent.putExtra("BitmapImage",bitmap);
					noMatchIntent.putExtra("deviceId",deviceId);
					startActivity(noMatchIntent);
					Intent noMatchIntent=new Intent(this,CrimeReportActivityEnroll.class);
					BitmapDrawable bitmapDrawable = ((BitmapDrawable) img_fp_src.getDrawable());
					Bitmap bitmap = bitmapDrawable.getBitmap();
					noMatchIntent.putExtra("BitmapImage",bitmap);
					startActivity(noMatchIntent);
					break;
				case DialogInterface.BUTTON_NEGATIVE: // no
					//t.setText("You have denied the TOS. You may not access the site");
					break;
				default:
					// nothing
					break;
				}
	}
	
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(moreButton.getText().equals("Get Criminal Info")){
			ProgressDialog progress = ProgressDialog.show(CrimeReportActivity.this, "Retrieving Information", "Please wait...",true);
			System.out.println("Entered into onclick method");
			if(criminalName!=null){
				Intent intent = new Intent(getApplicationContext(),ShowCirminalInfo.class);
			    intent.putExtra("criminalName",criminalName);
			    intent.putExtra("templateURL", templateURL);
			    intent.putExtra("address", address);
			    intent.putExtra("phoneNo", phoneNo);
			    intent.putExtra("latitude",latitude);
			    intent.putExtra("longtitude",longtitude);
			    intent.putExtra("personCategory",personCategory);
			    intent.putExtra("webImage",webImage);
			    startActivity(intent);
			    finish();
			    
			}
		}else{
			AlertDialog ad = new AlertDialog.Builder(this)
			.setMessage("Do you want to Enroll?")
			.setIcon(R.drawable.ic_launcher)
			.setTitle("Enroll Fingerprint")
			.setPositiveButton("Yes", this)
			.setNegativeButton("Cancel", this)
		    .setCancelable(false)
			.create();
			ad.show();
		}
	 //end of if
	}
}*/