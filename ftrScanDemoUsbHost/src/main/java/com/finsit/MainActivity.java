package com.finsit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.finsit.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends Activity
{
    private GoogleMap mMap;
    private ArrayList<MyMarker> mMyMarkersArray = new ArrayList<MyMarker>();
    private HashMap<Marker, MyMarker> mMarkersHashMap;
    private MyMarker selectedMaker;
    private Marker marker;
    double latitude;
    double longitude;
    double latitude1;
    double longitude1;
    boolean mapClickable=true;
    GPSTracker gps;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectmarker);
        setUpMap();
        Button setMapButton= (Button)findViewById(R.id.setRangeButton);
        mMarkersHashMap = new HashMap<Marker, MyMarker>();
        Button setCurrentLocation= (Button)findViewById(R.id.setCurrentLocation);
        Intent intent = getIntent();
        if(getIntent().getExtras() != null){
        	mapClickable=false;
        	System.out.println("Entered plot marker criminal information mapview");
        	String crimeLatittude=intent.getStringExtra("latitude");
        	String crimeLongtitude=intent.getStringExtra("longtitude");
        	String name=intent.getStringExtra("name");
        	System.out.println("crime latitude:"+crimeLatittude+" Crime Longtitude:"+crimeLongtitude);
        	ArrayList<MyMarker> crimeMarker=new ArrayList<MyMarker>();
        	MyMarker recievedMarker=new MyMarker();
        	recievedMarker.setmLabel(name);
        	recievedMarker.setmLatitude(Double.parseDouble(crimeLatittude));
        	recievedMarker.setmLongitude(Double.parseDouble(crimeLongtitude));
        	crimeMarker.add(recievedMarker);
        	plotMarkers(crimeMarker);
            setCurrentLocation.setVisibility(android.view.View.INVISIBLE);
            setMapButton.setVisibility(android.view.View.INVISIBLE);
            
        }else{
        	mapClickable=true;
            // Initialize the HashMap for Markers and MyMarker object
            
     /*     mMyMarkersArray.add(new MyMarker("Brasil", "icon", Double.parseDouble("-28.5971788"), Double.parseDouble("-52.7309824")));
            mMyMarkersArray.add(new MyMarker("United States", "icon2", Double.parseDouble("33.7266622"), Double.parseDouble("-87.1469829")));
            mMyMarkersArray.add(new MyMarker("Canada", "icon3", Double.parseDouble("51.8917773"), Double.parseDouble("-86.0922954")));
            mMyMarkersArray.add(new MyMarker("England", "icon4", Double.parseDouble("52.4435047"), Double.parseDouble("-3.4199249")));
            mMyMarkersArray.add(new MyMarker("España", "icon5", Double.parseDouble("41.8728262"), Double.parseDouble("-0.2375882")));
            mMyMarkersArray.add(new MyMarker("Portugal", "icon6", Double.parseDouble("40.8316649"), Double.parseDouble("-4.936009")));
            mMyMarkersArray.add(new MyMarker("Deutschland", "icon7", Double.parseDouble("51.1642292"), Double.parseDouble("10.4541194")));
            mMyMarkersArray.add(new MyMarker("Atlantic Ocean", "icondefault", Double.parseDouble("-13.1294607"), Double.parseDouble("-19.9602353")));
    */
            
            setMapButton.setOnClickListener(new View.OnClickListener() {
     		@Override
     		public void onClick(View v) {
     			// TODO Auto-generated method stub

     			Intent intent=new Intent();  
                
     			 intent.putExtra("LATITUDE",latitude+"");
     			 intent.putExtra("LONGITUDE",longitude+"");
    			 
                setResult(RESULT_OK,intent); 
     			finish();
     		}
     	});
            //For setting the current location

            setCurrentLocation.setOnClickListener(new View.OnClickListener() {
     		
     		@Override
     		public void onClick(View v) {
     			// TODO Auto-generated method stub
    	        gps = new GPSTracker(MainActivity.this);
    			// check if GPS enabled		
    	        if(gps.canGetLocation()){
    	        	latitude1 = gps.getLatitude();
    	        	longitude1 = gps.getLongitude();
    	        	System.out.println("Current location is:"+latitude1+","+longitude1);
    	        	Intent intent=new Intent();  
    	 			 intent.putExtra("LATITUDE",latitude1+"");
    	 			 intent.putExtra("LONGITUDE",longitude1+"");
    	 			 setResult(RESULT_OK,intent); 
    	 			 finish();
    	        	// \n is for new line
    	        	Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude1 + "\nLong: " + longitude1, Toast.LENGTH_LONG).show();	
    	        }else{
    	        	// can't get location
    	        	// GPS or Network is not enabled
    	        	// Ask user to enable GPS/network in settings
    	        	gps.showSettingsAlert();
    	        }
    			
     			 
     		}
     	});
        }

        plotMarkers(mMyMarkersArray);
    }

    private void plotMarkers(ArrayList<MyMarker> markers)
    {
        if(markers.size() > 0)
        {
            for (MyMarker myMarker : markers)
            {
                // Create user marker with custom icon and other options
            	System.out.println("Entered into plot markers method");
                MarkerOptions markerOption = new MarkerOptions().position(new LatLng(myMarker.getmLatitude(), myMarker.getmLongitude()));
                markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.currentlocation_icon));
                Marker currentMarker = mMap.addMarker(markerOption);
                mMarkersHashMap.put(currentMarker,myMarker);
                mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(myMarker.getmLatitude(),myMarker.getmLongitude()) , 14.0f) );
            }
        }
        
      
        
    }

    private int manageMarkerIcon(String markerIcon)
    {
        if (markerIcon.equals("icon"))
            return R.drawable.icon;
        else if(markerIcon.equals("icon2"))
            return R.drawable.icon2;
        else if(markerIcon.equals("icon3"))
            return R.drawable.icon3;
        else if(markerIcon.equals("icon4"))
            return R.drawable.icon4;
        else if(markerIcon.equals("icon5"))
            return R.drawable.icon5;
        else if(markerIcon.equals("icon6"))
            return R.drawable.icon6;
        else if(markerIcon.equals("icon7"))
            return R.drawable.icon7;
        else
            return R.drawable.icondefault;
    }


    private void setUpMap()
    {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null)
        {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

            // Check if we were successful in obtaining the map.

            if (mMap != null)
            {
/*                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
                {
                    @Override
                    public boolean onMarkerClick(com.google.android.gms.maps.model.Marker marker)
                    {
                        marker.showInfoWindow();
                        return true;
                    }
                });*/
            }
            else
                Toast.makeText(getApplicationContext(), "Unable to create Maps", Toast.LENGTH_SHORT).show();
            
            mMap.getUiSettings().setZoomControlsEnabled(true);
            if(mapClickable){
                mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
    				@Override
    				public void onMapClick(LatLng arg0) {
    					// TODO Auto-generated method stub
    					 Toast.makeText(getApplicationContext(), "Map Clicked:" + arg0.latitude+","+arg0.longitude, Toast.LENGTH_SHORT).show();
    				     latitude=arg0.latitude;
    				     longitude=arg0.longitude;
    					 if(marker!=null)
    					 {
    						 marker.remove();
    					 }
    				     MarkerOptions markerOption = new MarkerOptions().position(new LatLng(arg0.latitude, arg0.longitude));
    		             markerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.currentlocation_icon));
    		             marker = mMap.addMarker(markerOption);
    		             //mMarkersHashMap.put(currentMarker, myMarker);
    		             //Map.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
    				}
    			});            	
            }
        }
    }

    public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter
    {
        public MarkerInfoWindowAdapter()
        {
        }

        @Override
        public View getInfoWindow(Marker marker)
        {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker)
        {
            View v  = getLayoutInflater().inflate(R.layout.infowindow_layout, null);

            MyMarker myMarker = mMarkersHashMap.get(marker);

            ImageView markerIcon = (ImageView) v.findViewById(R.id.marker_icon);

            TextView markerLabel = (TextView)v.findViewById(R.id.marker_label);

            TextView anotherLabel = (TextView)v.findViewById(R.id.another_label);

            markerIcon.setImageResource(manageMarkerIcon(myMarker.getmIcon()));

            markerLabel.setText(myMarker.getmLabel());
            anotherLabel.setText("A custom text");

            return v;
        }
    }
}
