/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.finsit;

public final class R {
    public static final class array {
        public static final int am_pm=0x7f070000;
        public static final int crimes=0x7f070001;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int genre=0x7f080000;
        public static final int list_divider=0x7f080001;
        public static final int list_row_end_color=0x7f080002;
        public static final int list_row_hover_end_color=0x7f080003;
        public static final int list_row_hover_start_color=0x7f080004;
        public static final int list_row_start_color=0x7f080005;
        public static final int year=0x7f080006;
    }
    public static final class dimen {
        public static final int activity_horizontal_margin=0x7f060000;
        public static final int activity_vertical_margin=0x7f060001;
        public static final int genre=0x7f060002;
        public static final int rating=0x7f060003;
        public static final int title=0x7f060004;
        public static final int year=0x7f060005;
    }
    public static final class drawable {
        public static final int back=0x7f020000;
        public static final int currentlocation_icon=0x7f020001;
        public static final int custom_button=0x7f020002;
        public static final int custom_button_grey=0x7f020003;
        public static final int edit_text=0x7f020004;
        public static final int face=0x7f020005;
        public static final int finger=0x7f020006;
        public static final int fingerbg=0x7f020007;
        public static final int fir=0x7f020008;
        public static final int ic_action_attach=0x7f020009;
        public static final int ic_action_call=0x7f02000a;
        public static final int ic_action_copy=0x7f02000b;
        public static final int ic_action_cut=0x7f02000c;
        public static final int ic_action_delete=0x7f02000d;
        public static final int ic_action_done=0x7f02000e;
        public static final int ic_action_edit=0x7f02000f;
        public static final int ic_action_locate=0x7f020010;
        public static final int ic_action_mail=0x7f020011;
        public static final int ic_action_mail_add=0x7f020012;
        public static final int ic_action_microphone=0x7f020013;
        public static final int ic_action_overflow=0x7f020014;
        public static final int ic_action_paste=0x7f020015;
        public static final int ic_action_photo=0x7f020016;
        public static final int ic_action_refresh=0x7f020017;
        public static final int ic_action_search=0x7f020018;
        public static final int ic_action_select_all=0x7f020019;
        public static final int ic_action_send=0x7f02001a;
        public static final int ic_action_share=0x7f02001b;
        public static final int ic_action_star=0x7f02001c;
        public static final int ic_action_user=0x7f02001d;
        public static final int ic_action_user_add=0x7f02001e;
        public static final int ic_action_video=0x7f02001f;
        public static final int ic_launcher=0x7f020020;
        public static final int icon=0x7f020021;
        public static final int icon1=0x7f020022;
        public static final int icon2=0x7f020023;
        public static final int icon3=0x7f020024;
        public static final int icon4=0x7f020025;
        public static final int icon5=0x7f020026;
        public static final int icon6=0x7f020027;
        public static final int icon7=0x7f020028;
        public static final int icondefault=0x7f020029;
        public static final int list_row_bg=0x7f02002a;
        public static final int list_row_bg_hover=0x7f02002b;
        public static final int list_row_selector=0x7f02002c;
        public static final int login=0x7f02002d;
        public static final int login_old=0x7f02002e;
        public static final int logo=0x7f02002f;
        public static final int no_image_available=0x7f020030;
        public static final int policebackground=0x7f020031;
        public static final int policebackground_ap=0x7f020032;
        public static final int policebackground_ap_copy=0x7f020033;
        public static final int smoke3=0x7f020034;
        public static final int splashsreen=0x7f020035;
        public static final int tracking1=0x7f020036;
    }
    public static final class id {
        public static final int ListView01=0x7f0b0079;
        public static final int TextView01=0x7f0b007b;
        public static final int TextView02=0x7f0b007c;
        public static final int TextView03=0x7f0b007d;
        public static final int TextView04=0x7f0b007e;
        public static final int action_settings=0x7f0b00a7;
        public static final int address=0x7f0b0083;
        public static final int another_label=0x7f0b0070;
        public static final int att=0x7f0b0085;
        public static final int att1=0x7f0b0087;
        public static final int att2=0x7f0b0089;
        public static final int att3=0x7f0b008a;
        public static final int att4=0x7f0b008b;
        public static final int att5=0x7f0b008d;
        public static final int backButton=0x7f0b0092;
        public static final int backButtonClose=0x7f0b0095;
        public static final int btnAutoOn1=0x7f0b0007;
        public static final int btnAutoOn2=0x7f0b0009;
        public static final int btnCapture1=0x7f0b0006;
        public static final int btnCapture2=0x7f0b0008;
        public static final int btnFaceSave=0x7f0b0058;
        public static final int btnOpenDevice=0x7f0b0005;
        public static final int btnScan=0x7f0b0056;
        public static final int btnStop=0x7f0b0057;
        public static final int btnVerifyIso=0x7f0b000a;
        public static final int btnVerifyRaw=0x7f0b000b;
        public static final int button1=0x7f0b000f;
        public static final int buttonClick=0x7f0b001f;
        public static final int buttonOK=0x7f0b0018;
        public static final int cancel=0x7f0b0031;
        public static final int category=0x7f0b0023;
        public static final int cbFrame=0x7f0b0053;
        public static final int cbInvertImage=0x7f0b0055;
        public static final int cbLFD=0x7f0b0054;
        public static final int cbUsbHostMode=0x7f0b005a;
        public static final int cname=0x7f0b001b;
        public static final int container=0x7f0b0097;
        public static final int crimeSpinner=0x7f0b003c;
        public static final int criminalImage=0x7f0b0080;
        public static final int criminalName=0x7f0b0082;
        public static final int deletebackup=0x7f0b00a2;
        public static final int description=0x7f0b0060;
        public static final int detailBox=0x7f0b003d;
        public static final int details=0x7f0b0073;
        public static final int deviceIdFromPCATS=0x7f0b005d;
        public static final int deviceid=0x7f0b004b;
        public static final int distanceedit=0x7f0b002b;
        public static final int distanceedittext=0x7f0b002a;
        public static final int district=0x7f0b0026;
        public static final int districtText=0x7f0b0025;
        public static final int editFileName=0x7f0b0017;
        public static final int editText1=0x7f0b000d;
        public static final int editText2=0x7f0b000e;
        public static final int enable=0x7f0b00a1;
        public static final int errortext=0x7f0b004c;
        public static final int exit_menu_item=0x7f0b00a9;
        public static final int genre=0x7f0b0077;
        public static final int imageFinger=0x7f0b005b;
        public static final int img_fp_dst=0x7f0b0003;
        public static final int img_fp_src=0x7f0b0002;
        public static final int ipedit=0x7f0b002f;
        public static final int ipedittext=0x7f0b002e;
        public static final int ipid=0x7f0b004d;
        public static final int issues=0x7f0b0021;
        public static final int latitude=0x7f0b008e;
        public static final int latitudeText=0x7f0b008f;
        public static final int latitude_show=0x7f0b0039;
        public static final int layout=0x7f0b001d;
        public static final int layout_editroot=0x7f0b0029;
        public static final int layout_main=0x7f0b009e;
        public static final int layout_root=0x7f0b004a;
        public static final int linearLayout1=0x7f0b0015;
        public static final int linearLayout2=0x7f0b0036;
        public static final int linearLayout4=0x7f0b0043;
        public static final int linearLayout44=0x7f0b0045;
        public static final int linearLayout45=0x7f0b0038;
        public static final int list=0x7f0b0011;
        public static final int listV_main=0x7f0b005c;
        public static final int listscreenshot=0x7f0b007a;
        public static final int loadFile=0x7f0b0035;
        public static final int longitude_show=0x7f0b003a;
        public static final int longtitude=0x7f0b0090;
        public static final int longtitudeText=0x7f0b0091;
        public static final int mainlayout=0x7f0b009b;
        public static final int map=0x7f0b0098;
        public static final int mapview=0x7f0b009c;
        public static final int marker_icon=0x7f0b006e;
        public static final int marker_label=0x7f0b006f;
        public static final int marker_layout=0x7f0b006c;
        public static final int menu_add_fir=0x7f0b00a3;
        public static final int menu_identify=0x7f0b00a4;
        public static final int menu_remove=0x7f0b00a5;
        public static final int menu_settings=0x7f0b00a6;
        public static final int menutext=0x7f0b009f;
        public static final int moreButton=0x7f0b005e;
        public static final int offline=0x7f0b0063;
        public static final int path=0x7f0b001c;
        public static final int personCategory=0x7f0b0081;
        public static final int phoneno=0x7f0b004e;
        public static final int photo=0x7f0b0071;
        public static final int powered=0x7f0b005f;
        public static final int preview=0x7f0b001e;
        public static final int punch=0x7f0b0062;
        public static final int qrscan=0x7f0b0061;
        public static final int radioBitmap=0x7f0b0013;
        public static final int radioGroup1=0x7f0b0012;
        public static final int radioWSQ=0x7f0b0014;
        public static final int rating=0x7f0b0076;
        public static final int reconnect=0x7f0b00a0;
        public static final int releaseYear=0x7f0b0078;
        public static final int report_issue=0x7f0b0020;
        public static final int retryUploadBtn=0x7f0b003f;
        public static final int rowtext=0x7f0b0096;
        public static final int save=0x7f0b0050;
        public static final int screenshot=0x7f0b0094;
        public static final int selectedFaceImageView=0x7f0b0037;
        public static final int selectedImageView=0x7f0b009d;
        public static final int setCurrentLocation=0x7f0b009a;
        public static final int setLocBtn=0x7f0b003b;
        public static final int setRangeButton=0x7f0b0099;
        public static final int settings_menu_item=0x7f0b00a8;
        public static final int showCrimeLocation=0x7f0b0093;
        public static final int showMyComplaints=0x7f0b001a;
        public static final int someview=0x7f0b0010;
        public static final int spinner4=0x7f0b0049;
        public static final int spinnerDay=0x7f0b0042;
        public static final int spinnerMonth=0x7f0b0041;
        public static final int spinnerYear=0x7f0b0040;
        public static final int startCaptureBtn=0x7f0b0033;
        public static final int startCaptureBtn2=0x7f0b0034;
        public static final int state=0x7f0b0028;
        public static final int stateText=0x7f0b0027;
        public static final int station_info_layout=0x7f0b006d;
        public static final int status=0x7f0b0074;
        public static final int textDevice=0x7f0b0001;
        public static final int textInfo=0x7f0b0004;
        public static final int textMessage=0x7f0b0019;
        public static final int textVer=0x7f0b0000;
        public static final int textView=0x7f0b0051;
        public static final int textView1=0x7f0b000c;
        public static final int textView11=0x7f0b0064;
        public static final int textView12=0x7f0b0066;
        public static final int textView13=0x7f0b0068;
        public static final int textView14=0x7f0b006a;
        public static final int textView2=0x7f0b0016;
        public static final int textView3=0x7f0b0084;
        public static final int textView4=0x7f0b0086;
        public static final int textView5=0x7f0b0088;
        public static final int textView6=0x7f0b0044;
        public static final int textView7=0x7f0b0047;
        public static final int textView8=0x7f0b008c;
        public static final int thumbnail=0x7f0b0075;
        public static final int time=0x7f0b004f;
        public static final int timeHour=0x7f0b0046;
        public static final int timeMin=0x7f0b0048;
        public static final int timeedit=0x7f0b002d;
        public static final int timeedittext=0x7f0b002c;
        public static final int tinNumber=0x7f0b0022;
        public static final int title=0x7f0b0072;
        public static final int trackbutton=0x7f0b0032;
        public static final int tvMessage=0x7f0b0059;
        public static final int tvScannerInfo=0x7f0b0052;
        public static final int type=0x7f0b0024;
        public static final int update=0x7f0b0030;
        public static final int uploadBtn=0x7f0b003e;
        public static final int user_address=0x7f0b0069;
        public static final int user_detail=0x7f0b006b;
        public static final int user_email=0x7f0b0065;
        public static final int user_name=0x7f0b0067;
        public static final int webImage=0x7f0b007f;
    }
    public static final class layout {
        public static final int activity_android__demo=0x7f030000;
        public static final int activity_login=0x7f030001;
        public static final int activity_main=0x7f030002;
        public static final int activity_select_file_format=0x7f030003;
        public static final int activity_success=0x7f030004;
        public static final int activity_tin_popup_dialog=0x7f030005;
        public static final int browser=0x7f030006;
        public static final int campreview=0x7f030007;
        public static final int choicepage=0x7f030008;
        public static final int custom_view_list=0x7f030009;
        public static final int editdialog=0x7f03000a;
        public static final int electionscreen=0x7f03000b;
        public static final int enrollface=0x7f03000c;
        public static final int entrydialog=0x7f03000d;
        public static final int errordialog=0x7f03000e;
        public static final int facemain=0x7f03000f;
        public static final int fingerprint_upload=0x7f030010;
        public static final int fingerprint_upload_enroll=0x7f030011;
        public static final int fins_options=0x7f030012;
        public static final int home=0x7f030013;
        public static final int infowindow_layout=0x7f030014;
        public static final int item_details_view=0x7f030015;
        public static final int list_row=0x7f030016;
        public static final int listview=0x7f030017;
        public static final int main=0x7f030018;
        public static final int mapsview=0x7f030019;
        public static final int more_information=0x7f03001a;
        public static final int row=0x7f03001b;
        public static final int selectmarker=0x7f03001c;
        public static final int showmap=0x7f03001d;
        public static final int splash=0x7f03001e;
        public static final int startpage=0x7f03001f;
        public static final int validation=0x7f030020;
    }
    public static final class menu {
        public static final int activity_android__demo=0x7f0a0000;
        public static final int activity_select_file_format=0x7f0a0001;
        public static final int login=0x7f0a0002;
        public static final int main=0x7f0a0003;
        public static final int main_menu=0x7f0a0004;
        public static final int success=0x7f0a0005;
    }
    public static final class string {
        public static final int action_settings=0x7f090000;
        public static final int app_name=0x7f090001;
        public static final int complaints=0x7f090002;
        public static final int creator=0x7f090003;
        public static final int hello=0x7f090004;
        public static final int hello_world=0x7f090005;
        public static final int home_details=0x7f090006;
        public static final int home_form_address=0x7f090007;
        public static final int home_form_detail=0x7f090008;
        public static final int home_form_email=0x7f090009;
        public static final int home_form_name=0x7f09000a;
        public static final int image_time=0x7f09000b;
        public static final int loadImage=0x7f09000c;
        public static final int menu_settings=0x7f09000d;
        public static final int select_image=0x7f09000e;
        public static final int setloc=0x7f09000f;
        public static final int stare_capture_image=0x7f090010;
        public static final int thanks=0x7f090011;
        public static final int time_Day=0x7f090012;
        public static final int time_month=0x7f090013;
        public static final int time_time=0x7f090014;
        public static final int time_year=0x7f090015;
        public static final int title_activity_android__demo=0x7f090016;
        public static final int title_activity_login=0x7f090017;
        public static final int title_activity_select_file_format=0x7f090018;
        public static final int title_activity_success=0x7f090019;
    }
    public static final class style {
        /** 
            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        
         */
        public static final int AppBaseTheme=0x7f050001;
        /**  All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f050000;
    }
    public static final class xml {
        public static final int device_filter=0x7f040000;
        public static final int device_filter_nitgen=0x7f040001;
    }
}
